<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public $guarded = [];

    public function presupuestos()
    {
        return  $this->hasMany('App\Presupuesto','cliente_id');      
    }

    
    public function ventas()
    {
        return  $this->hasMany('App\Venta','cliente_id');      
    }

    public function cantidadPresu()
    {
        $cantidad = count($this->presupuestos);
       
        return $cantidad;
    }

    public function cantidadVentas()
    {
        $cantidad = count($this->ventas);
       
        return $cantidad;
    }
    
}
