<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrega extends Model
{
    public $guarded = [];

    public function venta()
    {
        return $this->belongsTo('App\Venta','venta_id');
    }

    

}
