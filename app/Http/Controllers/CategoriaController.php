<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Producto;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();
        return view('categorias/lista', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorias/alta');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $reglas = [
            'name' => "string|min:3",
            
        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"
        ];

        $this->validate($request, $reglas, $mensajes);


        $categoria = new Categoria;

        $categoria->nombre = $request["name"];
        $categoria->save();

       

        return redirect()->route('listaCategorias')->with('status','La categoría se registro exitosamente!'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        
        $productos = Producto::where('categoria_id','=',"$categoria->id")->where('mostrar','=',null)->orderBy('created_at', 'desc')->paginate(15);
        $categorias = Categoria::all();
       
        return view('productos/lista',compact('categoria','productos','categorias'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Categoria::find($id);
        return view('categorias/modificar',compact('categoria','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reglas = [
            'nombre' => "string|min:3",
            
        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"
        ];
        
        $this->validate($request, $reglas, $mensajes);

        $categoria = Categoria::find($id);
       
        $categoria->update([

            'nombre' => request()->nombre

        ]);


        return redirect()->route('listaCategorias')->with('status','La categoría ha sido modificada');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $item)
    {
        if(count($item->productos) == 0)
        {
            $item->delete();
        
        return redirect()->route('listaCategorias')->with('status','La categoría ha sido eliminada');
        } 
        else
        {
            return redirect()->route('listaCategorias')->with('no','La categoría no puede ser eliminada. Posee al menos un producto relacionado.');
        }
    }
}
