<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Presupuesto;
use App\Venta;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::orderBy('created_at', 'desc')->paginate(15);
        
        return view('clientes/lista',compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes/alta');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = new Cliente;

        $cliente->nombre = request()->nombre;
        $cliente->cuit = request()->cuit;
        $cliente->direccion = request()->direccion;
        $cliente->email = request()->email;
        $cliente->banco = request()->banco;
        $cliente->cbu = request()->cbu;
        $cliente->alias = request()->alias;
        $cliente->telefono = request()->telefono;

        $cliente->save();


        
        return redirect()->route('listaClientes')->with('status','Cliente guardado');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        
        return view('clientes/cliente',compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        return view('clientes/modificar',compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {

        

        $cliente->update([
            'nombre' => request()->nombre,
            'direccion' => request()->direccion,
            'email' => request()->email,
            'cuit' => request()->cuit,
            'cbu' => request()->cbu,
            'alias' => request()->alias,
            'telefono' => request()->telefono,
            'banco' => request()->banco
        ]);

        

        return redirect()->route('verCliente',$cliente)->with('status','Cliente modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {

        foreach($cliente->presupuestos as $presupuesto)
        {
            $presupuesto->cliente_id = null;
            $presupuesto->save();
        }

        foreach($cliente->ventas as $venta)
        {
            $venta->cliente_id = null;
            $venta->save();
        }

        $cliente->delete();
        return redirect()->route('listaClientes')->with('status','Cliente eliminado.');
    }



    public function search(Request $request, Presupuesto $presupuesto = null)
    {
        $busqueda = $request->busqueda;

        $clientes = Cliente::where('nombre','LIKE',"%$busqueda%")->paginate(15);; 
        
        if($presupuesto == null) {
            return view('clientes/lista',compact('clientes'));
        }
        
        return view('clientes/busqueda',compact('clientes','presupuesto'));
    }

    public function agregarClientePresupuesto(Presupuesto $presupuesto,Cliente $cliente)
    {
       
       $presupuesto->update([
           'cliente_id' => $cliente->id
       ]);
     

      return redirect()->route('verPresupuesto',$presupuesto->id)->with('status','Cliente asignado al presupuesto.');
    }

    public function quitarClientePresupuesto(Presupuesto $presupuesto)
    {
        $presupuesto->cliente_id = null;
        $presupuesto->save();
        return redirect()->route('verPresupuesto',$presupuesto->id)->with('status','Presupuesto actualizado.');
    }

    public function ventasCliente(Cliente $cliente)
    {
        $ventas = $cliente->ventas;

        $ventas = Venta::where('cliente_id','=',"$cliente->id")->orderBy('created_at', 'desc')->paginate(15);
           
       
        
        return view('ventas/lista',compact('ventas'));
    }

    public function presupuestosCliente(Cliente $cliente)
    {
        $presupuestos = Presupuesto::where('cliente_id','=',"$cliente->id")->orderBy('created_at', 'desc')->paginate(15);
        
        return view('presupuestos/lista',compact('presupuestos'));
    }

    
}
