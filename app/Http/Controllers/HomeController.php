<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venta;
use App\Moneda;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $moneda = Moneda::where('nombre','=','Dolar')->get();
        $moneda = $moneda[0];
        $aFacturar = count(Venta::where('facturado','=',false)->get());
       
        $cuentasConSaldoPendiente  = count(Venta::where('saldado','=','0')->get());
        
        return view('home',compact('aFacturar','cuentasConSaldoPendiente','moneda'));
    }

    public function facturacionPendiente()
    {
        
        $ventas = Venta::where('facturado','=',false)->paginate(15);
        
        return view('ventas/lista',compact('ventas'));
    }

    public function update(Request $request, Moneda $moneda)
    {

        $reglas = [
            'valor' => "numeric|min:1",
        ];
        $mensajes = [            
            'numeric' => "El campo :attribute debe ser un numero",        
            'min' => "El campo :attribute debe tener un mínimo de :min",
            'max' => "El campo :attribute debe tener un máximo de :max"
        ];

        $this->validate($request, $reglas, $mensajes);

        $moneda->update([
            'valor' => $request['valor']
        ]);

        
        return redirect()->route('home')->with('dolar','Valor modificado.');
    }

    
}
