<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notificacion;
use App\Venta;
use Session;

class NotificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notificacion = new Notificacion;
        $notificacion->venta_id = Session::get('id_venta');
        $notificacion->descripcion = $request->descripcion;
        
        # Comentado para incorporar en caso de necesitar agregar recordatorio de aviso segun fecha
        /* $notificacion->fecha_aviso = $request->fecha; */

        $notificacion->save();

        $venta = Venta::find($notificacion->venta_id);
        
        return redirect(url()->previous())->with('status','Aviso guardado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Venta $venta)
    {
        $venta->notificaciones->first()->delete();
        return redirect()->back()->with('status','Notificación eliminada.');
    }

    public function obtenerVenta($idVenta)
    {  
        Session::flash('id_venta', $idVenta);
    }
}
