<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Cart;
use App\Producto;
use App\Presupuesto;
use App\Item;
use App\TipoCliente;
use App\Moneda;
use App\Cliente;
use Barryvdh\DomPDF\Facade as PDF;

class PresupuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {

        $presupuestos = Presupuesto::all()->sortByDesc('created_at');

        
        return view('presupuestos/lista',compact('presupuestos'));

    }

    
    public function add(Request $request, Producto $producto)
    {

       


        /*  */        
        switch(request()->tipoCliente)
        {
            case 'cf':
                $precioTipoCliente = $producto->precio;
                break;
            case 'm':
                $precioTipoCliente = $producto->precioMayorista;
                break;
            case 'v':
                $precioTipoCliente = $producto->precioVidriero;
        }
        /*  */

        $cantidad = $producto->metros($producto->categoria->nombre,$request->cantidad);
        

        if($producto->stock >= $cantidad/* $request->cantidad */)
        {
            
     
                

            

                $add = Cart::add([

                    'id' => $producto->id,
                    'name' => $producto->nombre,                
                    'price' => $precioTipoCliente,
                    'weight' => 0,
                    'qty' => $cantidad/* $request["cantidad"] */,
                    'options' => [
                        'foto' => $producto->foto,
                        'descripcion' => $producto->descripcion,
                        'moneda' => $producto->moneda,
                        'tipocliente' => request()->tipoCliente
                    ] 
                ]);

            
                return redirect()->route('listaProductos')->with('status','Producto agregado a presupuesto.'); 
                
        }

        else

        {

            $add = Cart::add([

                'id' => $producto->id,
                'name' => $producto->nombre,
                'price' => $precioTipoCliente,
                'weight' => 0,
                'qty' => $cantidad/* $request["cantidad"] */,
                'options' => [
                    'foto' => $producto->foto,
                    'descripcion' => $producto->descripcion,
                    'moneda' => $producto->moneda,
                    'tipocliente' => request()->tipoCliente

                ] 
            ]);

            return redirect()->route('listaProductos')->with('no','Agregado a presupuesto. Revisar stock del artículo.'); 
        }
        
     
        
    }

    public function updateItem($rowId,Request $request)
    {
        
        Cart::update($rowId, $request->cantidad);    
        return  redirect()->route('presupuesto')->with('status','Presupuesto actualizado');
    }

    public function updateItemPresupuesto(Request $request, Item $item)
    {

        $status = 'status';
        $mensaje = 'Item modificado.';
        
           if($request["cantidad"] > $item->producto->stock)
           {
            $mensaje = "Item modificado. Revisar stock.";    
            $status = 'no';

           }

        
        $item->update([
            'precio' => request()->precio,
            'cantidad' => request()->cantidad
        ]);

        return redirect()->route('verPresupuesto',$item->presupuesto->id)->with($status,$mensaje);
    }

    public function agregarProductoProvisorio(Request $request)
    {

        
        $add = Cart::add([

            'id' => str_shuffle('ABCDEFGH'),
            'name' => request()->nombre,
            'price' => request()->precio,
            'weight' => 0,
            'qty' => request()->cantidad,
            'options' => [
                'descripcion' => request()->descripcion,
                'moneda' => 'Peso'
            ] 
        ]);
       

        return redirect()->route('presupuesto')->with('status','Producto agregado a presupuesto.'); 

    }

    public function agregarProductoProvisorioAPresupuesto(Request $request,$presupuestoId)
    {
        
        
        $producto = new Producto;
        $producto->nombre = request()->nombre;
        $producto->descripcion = request()->descripcion;
        $producto->precio = request()->precio;
        $producto->stock = 1000;
        $producto->foto = "SinFoto.jpeg";
        $producto->categoria_id = "1";
        $producto->mostrar = "no";
        $producto->moneda = "Peso";
        $producto->save();

        $item = new Item;
        $item->producto_id = $producto->id;
        $item->presupuesto_id = $presupuestoId;
        $item->cantidad = $producto->precio;
        $item->precio = $producto->precio*request()->cantidad;
        $item->save();


        return  redirect()->route('verPresupuesto',$presupuestoId)->with('status','Presupuesto actualizado');


    }
    
    
    
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

        public function store(Request $request)
        {
            
            
           
        
            
            $dolar = Moneda::where('nombre','=','Dolar')->get();
            $dolar = $dolar[0];

        
            if($request->nombreCliente == null)
            {
                $request->nombreCliente = "-";
            }

            if($request->descripcion == null)
            {
                $request->descripcion = "-";
            }

            if($request->email == null)
            {
                $request->email = "-";
            }

            if($request->adicional == null)
            {
                $request->adicional = 0;
            }
            
        

            $presupuesto = new Presupuesto;
            $presupuesto->nombreCliente = $request->nombreCliente;
            $presupuesto->vendedor = Auth()->user()->id; 
            $presupuesto->totalPresupuesto = 0; 
            $presupuesto->descripcion = $request->descripcion;
            $presupuesto->email = $request->email;
            $presupuesto->iva = $request->iva;
            $presupuesto->recargo = $request->recargo;
            $presupuesto->adicional = $request->adicional;
            $presupuesto->tipocliente_id = $request->tipoCliente;
            $presupuesto->cliente_id = null;
            /*  */
            $presupuesto->valorDolar = $dolar->valor;
            /*  */

            $presupuesto->save();
            
            foreach(Cart::content() as $producto )
            {
                
                if(!is_int($producto->id))
                {
                    
                    $productoAux = new Producto;
                    $productoAux->nombre = $producto->name;
                    
                    $productoAux->precio = $producto->price;
                    $productoAux->descripcion = $producto->options->descripcion;
                    $productoAux->moneda = $producto->options->moneda;
                    $productoAux->categoria_id = "1";
                    $productoAux->foto = "SinFoto.jpeg";
                    $productoAux->stock = 1000;   
                    $productoAux->mostrar = 'no';                 
                    $productoAux->save();
                    
                    $producto->id = $productoAux->id;

                }
               
                $item = new Item;
                $item->presupuesto_id = $presupuesto->id;
                $item->producto_id = $producto->id;
                $item->cantidad = $producto->qty;
                $item->tipocliente = $producto->options->tipocliente;

                if($producto->options->moneda == 'Dolar')
                {
                    /* $item->precio = $producto->price * $dolar->valor; */
                    $item->precio = $producto->price * $presupuesto->valorDolar;
                }
                else
                {
                    $item->precio = $producto->price;
                }
                
                $item->save();
    
            }

            Cart::destroy(); 
            

            return redirect()->route('listaPresupuestos')->with('status','Presupuesto generado.');       

        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show()
    {               
        $dolar = Moneda::where('nombre','=','Dolar')->get();
        $dolar = $dolar[0];
        $tiposcliente = TipoCliente::all();
        
        $presupuesto = Cart::content();
    
        return view('presupuestos/presupuesto',compact('presupuesto','dolar','tiposcliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        

        $presupuestoId = $item->presupuesto->id;
        $presupuesto = Presupuesto::find($presupuestoId);
        
        if($item->venta_id == null)
        {            
            $item->delete();                        
        }
        else
        {
            $idPresupuesto = $item->presupuesto_id;
            $item->presupuesto_id = null;
            $item->save();
        }

        if( count($item->presupuesto->items) == 0)
        {
            $presupuesto->delete();
            return  redirect()->route('listaPresupuestos')->with('status','Presupuesto eliminado.');
        }
        
        return  redirect()->route('verPresupuesto',$presupuestoId)->with('status','Presupuesto actualizado');

    }

    public function remover($item)
    {

        Cart::remove($item);
        return  redirect()->route('presupuesto')->with('status','Presupuesto actualizado');

    }

    public function eliminarPresupuesto($id)
    {
        $presupuesto = Presupuesto::find($id);


        foreach($presupuesto->items as $item)
        {
            
            $item->presupuesto_id = null;

            if($item->presupuesto_id == null && $item->venta_id == null)
            {
                $item->delete();
            }
            else
            {
                $item->save();
            }            

        }

       

        
        $presupuesto->delete();
        

        return redirect()->route('listaPresupuestos')->with('status','Presupuesto eliminado.');

        
    }


    public function pdf($id)
    {
        $presupuesto = Presupuesto::find($id);
        $pdf = PDF::loadView('presupuestos/pdf',compact('presupuesto'));        
        return $pdf->stream();


    }

    public function presupuesto($id)
    {
        
        
        $presupuesto = Presupuesto::find($id);
        $cliente = $presupuesto;
        $presupuesto = $presupuesto->items;
        $tiposcliente = TipoCliente::all();
        
        
       if(count($presupuesto) > 0)
       {
           $tieneItems = true;
       }
       else
       {
           $tieneItems = false;
       }

        return view('presupuestos/show',compact('presupuesto','cliente','tieneItems','tiposcliente'));        
        

    }


    


    public function updatePresupuesto(Request $request,$presupuestoId)
    {

        
        
        $presupuesto = Presupuesto::find($presupuestoId);

        $mensaje = 'Presupuesto actualizado.';
        $status = 'status';
        $productosSinStock = [];

  

        if($presupuesto->nombreCliente != $request["nombreCliente"])
        {
            $presupuesto->nombreCliente = $request["nombreCliente"];
        }

        if($presupuesto->email != $request["email"])
        {
            $presupuesto->email = $request["email"];
        }

        if($presupuesto->descripcion != $request["descripcion"])
        {
            $presupuesto->descripcion = $request["descripcion"];
        }

        if($presupuesto->iva != $request["iva"])
        {
            $presupuesto->iva = $request["iva"];
        }

        if($presupuesto->recargo != $request["recargo"])
        {
            $presupuesto->recargo = $request["recargo"];
        }

        if($presupuesto->adicional != $request["adicional"])
        {
            $presupuesto->adicional = $request["adicional"];
        }

        if($presupuesto->tipocliente_id != $request["tipoCliente"])
        {
            $presupuesto->tipocliente_id = $request["tipoCliente"];
        }

        if($presupuesto->valorDolar != $request["valorDolar"])
        {
            $presupuesto->valorDolar = $request["valorDolar"];
            
            

            foreach($presupuesto->items as $item)
            {
                
                if($item->producto->moneda == 'Dolar')
                {
                    
                  
                    switch($item->tipocliente)
                    {
                        case 'cf':
                            /* $item->precio = ($item->precio/$item->cantidad)*$presupuesto->valorDolar; */
                            $item->precio = $presupuesto->valorDolar * $item->producto->precio;
                            break;
                        case 'm':
                            /* $item->precio = ($item->precio/$item->cantidad)*$presupuesto->valorDolar; */
                            $item->precio = $presupuesto->valorDolar * $item->producto->precioMayorista;
                            break;
                        case 'v':
                            /* $item->precio = ($item->precio/$item->cantidad)*$presupuesto->valorDolar; */
                            $item->precio = $presupuesto->valorDolar * $item->producto->precioVidriero; 
                            break;                   
                    }
                    
                    $item->save();
       
                }
               
            }
           
        }

        

        $presupuesto->save();


        $cliente = $presupuesto;
        $presupuesto = $presupuesto->items;
       

        return  redirect()->route('verPresupuesto',$cliente->id)->with('status','Presupuesto actualizado.');

    }


    public function addProducto(Producto $producto, Presupuesto $presupuesto, Request $request)
    {
        
        $productoNuevo = true;

        

        foreach($presupuesto->items as $items => $item)
        {
            

            if($item->producto_id == $producto->id)
            {
                $item->update([
                    'cantidad' => request()->cantidad + $item->cantidad
                ]);

                $productoNuevo = false;

                if(/* $item->cantidad */ request()->cantidad > $producto->stock)
                {
                    $mensaje = "Presupuesto actualizado. Revisar stock. ";
                    $status = "no";
                }
                else
                {
                    $mensaje = "Presupuesto actualizado.";
                    $status = "status";
                }
                
            }                    
            
        }

            if($productoNuevo == true)
            {
                $item = new Item;
                $item->cantidad = $request->cantidad;   
                $item->tipocliente = request()->tipoCliente;
                
                if($producto->moneda == 'Dolar')
                {
                                      
                    switch($item->tipocliente)
                    {
                        case 'cf':
                            $item->precio = $presupuesto->valorDolar * $producto->precio;
                            break;
                        case 'm':
                            $item->precio = $presupuesto->valorDolar * $producto->precioMayorista;
                            break;
                        case 'v':
                            $item->precio = $presupuesto->valorDolar * $producto->precioVidriero; 
                            break;                   
                    }
                    
       
                }
                else
                {
                    switch($item->tipocliente)
                    {
                        case 'cf':
                            $item->precio = $producto->precio;
                            break;
                        case 'm':
                            $item->precio = $producto->precioMayorista;
                            break;
                        case 'v':
                            $item->precio = $producto->precioVidriero; 
                            break;                   
                    }
                }
                
                /*  */

                $item->producto_id = $producto->id;
                $item->presupuesto_id = $presupuesto->id;
                

                $item->save(); 

                if($item->cantidad > $producto->stock)
                {
                    $mensaje = "Presupuesto actualizado. Revisar stock.";
                    $status = "no";
                }
                else
                {
                    $mensaje = "Presupuesto actualizado.";
                    $status = "status";
                }

            }
                
        return  redirect()->route('listaProductosPresupuestoGuardado',$presupuesto->id)->with($status,$mensaje);

    }


    
}
