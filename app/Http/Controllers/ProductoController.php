<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Categoria;
use App\Presupuesto;
use App\Moneda;
use App\Venta;
use Redirect;
use Session;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();
        $productos = Producto::orderBy('codigoProducto', 'asc')->where('mostrar','=',null)->paginate(15);
        return view('productos/lista',compact('productos','categorias'));
    }

    public function indexPresupuestoGuardado(Presupuesto $presupuesto)
    {
        
        
        $categorias = Categoria::all();
        $productos = Producto::orderBy('codigoProducto', 'asc')->where('mostrar','=',null)->paginate(15);
        
        return view('productos/listaParaPresupuesto',compact('productos','categorias','presupuesto'));
        
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();
        $monedas = Moneda::all();       
        return view('productos/alta',compact('categorias','monedas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $reglas = [
            'nombre' => "string|min:3",
            'descripcion' => "string|min:3",
            'precio' => "numeric|min:0",
            'categoria_id' => "required",
            'stock' => "numeric|min:0",
            'productoimagen' => "image"
        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"
        ];
        $this->validate($request, $reglas, $mensajes);
                

        $ruta = $request->file("productoimagen")->store("public");
        $nombreImagen = basename($ruta);

        $producto = new Producto;
        $producto->nombre = $request["nombre"];
        $producto->precio = $request["precio"];
        $producto->stock = $request["stock"];
        $producto->categoria_id = $request["categoria_id"];
        $producto->descripcion = $request["descripcion"];
        $producto->moneda = $request['moneda'];       
        $producto->foto =  $nombreImagen;
        $producto->precio = $request['precio'];
        $producto->precioVidriero = $request['precioVidriero'];
        $producto->precioMayorista = $request['precioMayorista'];
        $producto->codigoProducto = $request['codigoProducto'];
        
        $producto->save();                

        
        
        return redirect()->route('listaProductos')->with('status','El producto se registro exitosamente!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $producto = Producto::find($id);
        return view('productos/producto',compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        Session::flash('backUrl', url()->previous());
        $categorias = Categoria::all();
        $monedas = Moneda::all();   
        return view('productos/modificar',compact('producto','categorias','monedas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {

        $reglas = [
            'nombre' => "string|min:3",
            'descripcion' => "string|min:3",
            'precio' => "numeric|min:0",
            'precioMayorista' => "numeric|min:0",
            'precioVidriero' => "numeric|min:0",
            'categoria_id' => "required",
            'stock' => "numeric|min:0",
            'productoimagen' => "image"
        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"
        ];

        $this->validate($request, $reglas, $mensajes);
        
        
        if(request()->file('productoimagen')){

            //Momenteano hasta que hagamos carrusel

            if($producto->imagenes){
                foreach($producto->imagenes as $imagen)
                {
                    Storage::delete("public/".$imagen->nombre);
                }               
            }
            //---

            $ruta = $request->file("productoimagen")->store("public");
            $nombreImagen = basename($ruta);
             
        }
        else
        {
            $nombreImagen = $producto->foto;
        }

        $producto->update([

            'nombre' => $request['nombre'],
            'precio' => $request['precio'],
            'precioMayorista' => $request['precioMayorista'],
            'precioVidriero' => $request['precioVidriero'],
            'stock' => $request['stock'],
            'categoria_id' => $request['categoria_id'],
            'descripcion' => $request['descripcion'],
            'moneda' => $request['moneda'],
            'codigoProducto' => $request['codigoProducto'],
            'foto' => $nombreImagen

        ]);
        
        return redirect(Session::get('backUrl'))->with('status','Producto modificado.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {


        if(count($producto->item) == 0)
        {
            $producto->delete();
        
        return redirect()->route('listaProductos')->with('status','La producto ha sido eliminado');
        } 
        else
        {
            return redirect()->route('listaProductos')->with('no','El producto no puede ser eliminado. Posee relación con otros elementos.');
        }

    
    }

    public function search(Request $request)
    {
        $req = $request["busqueda"];
        $productos  = Producto::where('nombre',"LIKE","%$req%")->where('mostrar','=',null)->orWhere('codigoProducto',"LIKE","%$req%")->paginate(15);    
        $categorias = Categoria::all();
            
        return view('productos/lista',compact('productos','categorias'));
    }

    public function searchParaPresupuestosGuardados(Request $request,Presupuesto $presupuesto)
    {
        $req = $request["busqueda"];
        $productos  =  Producto::where('nombre',"LIKE","%$req%")->where('mostrar','=',null)->orWhere('codigoProducto',"LIKE","%$req%")->paginate(15);    
        $categorias = Categoria::all();
            
        return view('productos/listaParaPresupuesto',compact('productos','presupuesto', 'categorias'));
    }

    public function searchParaVentas(Request $request,Venta $venta)
    {
        $req = $request["busqueda"];
        $productos  =  Producto::where('nombre',"LIKE","%$req%")->where('mostrar','=',null)->orWhere('codigoProducto',"LIKE","%$req%")->paginate(15);    
        $categorias = Categoria::all();
        
            
        return view('productos/listaParaVenta',compact('productos','venta', 'categorias'));
    }



   

    
}
