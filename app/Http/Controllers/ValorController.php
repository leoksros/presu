<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Moneda;

class ValorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moneda = Moneda::where('nombre','=','Dolar')->get();
        $moneda = $moneda[0];
        
        return view('home',compact('moneda'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Moneda $moneda)
    {

        $reglas = [
            'valor' => "numeric|min:1",
        ];
        $mensajes = [            
            'numeric' => "El campo :attribute debe ser un numero",        
            'min' => "El campo :attribute debe tener un mínimo de :min",
            'max' => "El campo :attribute debe tener un máximo de :max"
        ];

        $this->validate($request, $reglas, $mensajes);

        $moneda->update([
            'valor' => $request['valor']
        ]);

        return redirect()->route('listaValores')->with('status','Valor modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
