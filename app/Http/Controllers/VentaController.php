<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Presupuesto;
use App\Item;
use App\Venta;
use App\Entrega;
use App\TipoCliente;
use App\Categoria;
use App\Producto;
use App\Notificacion;
use Barryvdh\DomPDF\Facade as PDF;


class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $ventas = Venta::orderBy('id', 'desc')->paginate(15);
        return view('ventas/lista',compact('ventas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Presupuesto $presupuesto)
    {
        $productosSinStock = [];

        foreach($presupuesto->items as $item)
        {
            
           if($item->cantidad > $item->producto->stock)
           {
            $productosSinStock[] = $item->producto->nombre;
           }
        }          

        if(count($presupuesto->items) > 0 && count($productosSinStock) == 0)
        {
            
            $venta = new Venta;
            $venta->nombreCliente = $presupuesto->nombreCliente;
            $venta->descripcion = $presupuesto->descripcion;
            $venta->totalVenta = $presupuesto->total();
            $venta->email = $presupuesto->email;
            $venta->cliente_id = $presupuesto->cliente_id;
            $venta->saldado = 0;        
            $venta->valorDolar = $presupuesto->valorDolar;

            $venta->iva = $presupuesto->iva;
            $venta->recargo = $presupuesto->recargo;
            $venta->tipocliente_id = $presupuesto->tipocliente_id;
            $venta->adicional = $presupuesto->adicional;

            if($presupuesto->iva == '21')
            {
                $venta->facturado = false;
            }
            else
            {
                $venta->facturado = true;
            }            

            /*  */
            $venta->save();

            foreach($presupuesto->items as $item)
            {
            $item->venta_id = $venta->id;
            $item->save();

            /* Actualización de stock */

                    $item->producto->stock = $item->producto->stock - $item->cantidad;
                    $item->producto->save();

            /*  */
            }

            return redirect()->route('listaVentas')->with('status','Venta registrada.');  

        }
        elseif(count($presupuesto->items) > 0)
        {
            $mensaje = "Stock insuficiente para registrar venta de: ";
            $status = 'error';

            foreach($productosSinStock as $producto)
            {
                $mensaje = $mensaje."-".$producto;
            }
            return redirect()->route('verPresupuesto',$presupuesto->id)->with($status,$mensaje);      
        }
        else
        {
            return redirect()->route('verPresupuesto',$presupuesto->id)->with('no','El presupuesto no posee productos.'); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venta = Venta::find($id);
        $tiposcliente = TipoCliente::all();
        
        return view('ventas/show',compact('venta','tiposcliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Venta $venta)
    {
        $url = parse_url(url()->previous())['path'];
        
        $entrega = new Entrega;

        $entrega->entrega = request()->entregaParcial;
        $entrega->venta_id = $venta->id;
        $entrega->detalle = request()->detalleEntrega;
        $entrega->save();
        
        $venta->saldado = $venta->controlEntregas();
        $venta->save();
        
        if($url == '/venta/detallesSaldo/'.$venta->id) {
            return  redirect()->route('detallesSaldo',$venta)->with('status','Entrega registrada.');
        }

        return  redirect('venta/'.$venta->id)->with('status','Entrega registrada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $venta = Venta::find($id);

        if(count($venta->entregas) > 0)
        {
            return redirect()->route('listaVentas')->with('no','La venta no puede ser eliminada.');  
        }
        
        foreach($venta->items as $item)
        {
            $item->venta_id = null;

            if($item->presupuesto_id == null &&  $item->venta_id == null)
            {
                $item->devolverStock();
                $item->delete();
            }
            else
            {
                $item->devolverStock();
                $item->save();
            }    
        }  
        $venta->delete();      
        return redirect()->route('listaVentas')->with('status','Venta eliminada.');  
    }

    public function pendientes($orden = null)
    {    
        
        if($orden == null) {
            $cuentas = Venta::where('saldado','=','0')->orderBy('created_at', 'desc')->paginate(15);
            return view('ventas/pendientes',compact('cuentas','orden'));
        }

        
        $cuentas = Venta::where('saldado','=','0')->orderBy('created_at', $orden)->paginate(15);   
        return view('ventas/pendientes',compact('cuentas','orden'));        
        
    }

    public function updateVenta(Request $request, Venta $venta)
    {

        $mensaje = "No hay stock suficiente de: ";
        $productosSinStock = [];    
        $items = $venta->items;

        if($venta->iva != $request["iva"])
        {
            $venta->iva = $request["iva"];

            if($venta->iva == '21')
            {
                $venta->facturado = false;
            }
            else
            {
                $venta->facturado = true;
            }      
        }

        if($venta->recargo != $request["recargo"])
        {
            $venta->recargo = $request["recargo"];
        }

        if($venta->adicional != $request["adicional"])
        {
            $venta->adicional = $request["adicional"];
        }

        if($venta->tipocliente_id != $request["tipoCliente"])
        {
            $venta->tipocliente_id = $request["tipoCliente"];
        }

        if($venta->valorDolar != $request["valorDolar"])
        { 
            $venta->valorDolar = $request["valorDolar"];                    
            foreach($venta->items as $item)
            {                    
                    if($item->producto->moneda == 'Dolar')
                    {                                        
                        switch($item->tipocliente)
                        {
                            case 'cf':
                                $item->precio = $venta->valorDolar * $item->producto->precio;
                                break;
                            case 'm':
                                $item->precio = $venta->valorDolar * $item->producto->precioMayorista;                                
                                break;
                            case 'v':
                                $item->precio = $venta->valorDolar * $item->producto->precioVidriero;                             
                                break;                   
                        }                    
                        $item->save();   

                    }
                }
        }
        $venta->save();
        return  redirect()->route('verVenta',$venta->id)->with('status','Venta actualizada');
    }


    public function updateItemVenta(Request $request, Item $item)
    {
        
        $unidades = 0;

        if(request()->cantidad > $item->cantidad)
        {
            $unidades = request()->cantidad - $item->cantidad;            
        }
        elseif(request()->cantidad < $item->cantidad)
        {

            $unidades = $item->cantidad - request()->cantidad;

            $item->producto->stock += $unidades;
            $item->producto->save();

            $item->update([
                    'cantidad' => request()->cantidad
            ]);        

            return redirect()->route('verVenta',$item->venta->id)->with('status','Item de venta actualizado.');

        }
        elseif(request()->cantidad == $item->cantidad)
        {
            return redirect()->route('verVenta',$item->venta->id)->with('status','Item de venta actualizado.');
        }

        
        if( $unidades <= $item->producto->stock )
        {
            
            $item->producto->stock = $item->producto->stock - $unidades;
            $item->producto->save();

            $item->update([
                'cantidad' => request()->cantidad
            ]);        
            
            return redirect()->route('verVenta',$item->venta->id)->with('status','Item de venta actualizado.');
        }
        else
        {
            return redirect()->route('verVenta',$item->venta->id)->with('no','No hay stock suficiente.');
        }

    }

           
                
   
    

    public function addProducto(Producto $producto, Venta $venta, Request $request)
    {
        
        
        $productoNuevo = true;
        
        

        foreach($venta->items as $items => $item)
        {
                                
            if($item->producto_id == $producto->id && $producto->stock >= ($request->cantidad + $item->cantidad))
            {
                    
                $item->update([
                    'cantidad' => request()->cantidad + $item->cantidad
                ]);


                $producto->stock = $producto->stock - $request["cantidad"];
                $producto->save();

                

                $productoNuevo = false;
                $mensaje = "Venta actualizada.";
                $status = "status";
                return  redirect()->route('listaProductosVenta',$venta->id)->with($status,$mensaje);

            }          
            elseif($item->producto_id == $producto->id && $producto->stock >= ($request->cantidad + $item->cantidad))
            {
                $mensaje = "Stock insuficiente.";
                $status = "no";
                return  redirect()->route('listaProductosVenta',$venta->id)->with($status,$mensaje);
            }                
                
        }    

        if($productoNuevo == true && $producto->stock >= $request->cantidad)
        {
                $item = new Item;
                    
                $item->cantidad = $request->cantidad;    
                                         
                
               
                if($producto->moneda == 'Dolar')
                {
                    

                    switch(request()->tipoCliente)
                    {
                        case 'cf':
                            $item->precio = $venta->valorDolar * $producto->precio;
                            break;
                        case 'm':
                            $item->precio = $venta->valorDolar * $producto->precioMayorista;
                            break;
                        case 'v':
                            $item->precio = $venta->valorDolar * $producto->precioVidriero; 
                            break;                   
                    }

                    
                }
                else
                {
                    switch(request()->tipoCliente)
                    {
                        case 'cf':
                            $item->precio = $producto->precio;
                            break;
                        case 'm':
                            $item->precio = $producto->precioMayorista;
                            break;
                        case 'v':
                            $item->precio = $producto->precioVidriero; 
                            break;                   
                    }
                }
                /*  */

                $item->producto_id = $producto->id;                
                $item->venta_id = $venta->id;
                                   
                $mensaje = "Venta actualizada.";
                $status = "status";
                $item->save();

                $producto->stock = $producto->stock - $request["cantidad"];
                $producto->save();


                return  redirect()->route('listaProductosVenta',$venta->id)->with($status,$mensaje);                
                        
        }
        elseif($productoNuevo == true && $producto->stock < $request->cantidad)
        {
            $mensaje = "Stock insuficiente.";
            $status = "no";              
            return  redirect()->route('listaProductosVenta',$venta->id)->with($status,$mensaje);  
        }

    }

    public function removerProducto(Item $item)
    { 
        $item->devolverStock();    
        $idVenta = $item->venta_id;
        $item->venta_id = null;
        $item->save();           
        return  redirect()->route('verVenta',$idVenta)->with('status','Venta actualizada');
    }

    public function listaProductos(Venta $venta)
    {
        $categorias = Categoria::all();
        $productos = Producto::orderBy('codigoProducto', 'asc')->paginate(15);
        return view('productos/listaParaVenta',compact('productos','categorias','venta'));
        
    }

    public function pdf($id)
    {
        $venta = Venta::find($id);
        $pdf = PDF::loadView('ventas/pdf',compact('venta'));        
        return $pdf->stream();

    }

    public function facturar(Venta $venta)
    {
        if($venta->facturado == false)
        {
            $venta->facturado = true;
            $venta->save();
            return redirect()->route('verVenta',$venta->id)->with('status','Venta facturada.');
        }
        else
        {
            $venta->facturado = false;
            $venta->save();
            return redirect()->route('verVenta',$venta->id)->with('status','Venta desfacturada.');
        }
        
    }

    public function verDetallesSaldo(Venta $venta)
    {
        return view('ventas.detalles',compact('venta'));
    }

    public function obtenerVentasNotificadas($orden = null)
    {
        $titulo = 'notificados';

        if($orden == null) {
            $ventas = Venta::has('notificaciones')->orderBy('created_at','desc')->paginate(15);
            return view('ventas.saldos',compact('ventas', 'titulo', 'orden'));
        }

        $ventas = Venta::has('notificaciones')->orderBy('created_at',$orden)->paginate(15);
        return view('ventas.saldos',compact('ventas', 'titulo', 'orden'));
    }

    public function obtenerVentasNoNotificadas($orden = null)
    {   
        $titulo = 'sin notificar';

        if($orden == null) {
            $ventas = Venta::doesnthave('notificaciones')->orderBy('created_at','desc')->paginate(15);
            return view('ventas.saldos',compact('cuentas','titulo','orden'));
        }

        $ventas = Venta::doesnthave('notificaciones')->orderBy('created_at',$orden)->paginate(15);
        return view('ventas.saldos',compact('ventas', 'titulo', 'orden'));
    }

}
