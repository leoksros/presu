<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Moneda;

class Item extends Model
{
    public $guarded = [];

    public function producto()
    {
        return $this->belongsTo("App\Producto","producto_id");
    }

    public function presupuesto()
    {
        return $this->belongsTo("App\Presupuesto","presupuesto_id");
    }

    public function venta()
    {
        return $this->belongsTo("App\Venta","venta_id");
    }

    public function valorItem()
    {
        
        return $this->precio*$this->cantidad;
    }

    public function devolverStock()
    {
        
        $this->producto->stock = ($this->producto->stock + $this->cantidad);
        $this->producto->save();
    }

}
