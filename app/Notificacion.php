<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table = 'notificaciones';
    protected $fillable = ['fecha_aviso','venta_id'];

    public function venta()
    {
        return $this->belongsTo("App\Venta","venta_id");
    }
}
