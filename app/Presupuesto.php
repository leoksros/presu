<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presupuesto extends Model
{
    public $guarded = [];

    
        public function items()
    {
        return $this->hasMany("App\Item","presupuesto_id");
    }


    public function tipocliente()
    {
        return $this->belongsTo("App\TipoCliente","tipocliente_id");
    }

    public function subtotal()
    {
            
        $total = 0;
        $total = $total + $this->adicional;

        foreach($this->items as $lineaDeCompra)
        {
            $total = $total + $lineaDeCompra->cantidad*$lineaDeCompra->precio;            
        }
        
        return $total;
    }

    public function total()
    {
            
        

        $recargo = $this->subtotal() * ($this->recargo/100);
        $subtotalConRecargo = $this->subtotal() + $recargo;


        return ($this->subtotal() + $recargo)+ ($subtotalConRecargo*$this->iva)/100;

        
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente','cliente_id');
    }
    

}
