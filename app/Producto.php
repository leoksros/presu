<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public $guarded = [];

    public function categoria()
    {
        return $this->belongsTo("App\Categoria","categoria_id");
    }

    public function item()
    {
        return $this->hasMany("App\Item",'producto_id');
    }

    public function metros($categoria,$cantidad)
    {
        if($categoria == "Vidrios" || $categoria == "Espejos")
            {
                $metros = explode(" ",$cantidad);
                $metrosCuadrados = 1;
                foreach($metros as $metro)
                {
                    $metrosCuadrados = $metro*$metrosCuadrados;
                }

                $cantidad = $metrosCuadrados; 
                
                return $cantidad;
            }

            if($categoria == "Tratamientos espejos")
            {
                $metros = explode(" ",$cantidad);
                $sumaMetrosLineales = 0;
                foreach($metros as $metro)
                {
                    $sumaMetrosLineales += $metro;
                }

                $cantidad = $sumaMetrosLineales;   

                return $cantidad;
            }

            return $cantidad;
            
    }

  

}
