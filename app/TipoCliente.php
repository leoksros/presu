<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCliente extends Model
{
    public $table = 'tipoclientes';
    public $guarded = [];
}
