<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Entrega;

class Venta extends Model
{
    public $guarded = [];

    public function items()
    {
        return $this->hasMany("App\Item","venta_id");
    }


    public function entregas()
    {
        return $this->hasMany('App\Entrega',"venta_id");
    }

    public function tipocliente()
    {
        return $this->belongsTo("App\TipoCliente","tipocliente_id");
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente','cliente_id');
    }

    public function notificaciones()
    {
        return $this->hasMany('App\Notificacion','venta_id');
    }


    public function subtotal()
    {
            
        $total = 0;
        $total = $total + $this->adicional;

        foreach($this->items as $lineaDeCompra)
        {
            $total = $total + $lineaDeCompra->cantidad*$lineaDeCompra->precio;            
        }
        
        return $total;
    }

    public function total()
    {
            
        

        $recargo = $this->subtotal() * ($this->recargo/100);
        $subtotalConRecargo = $this->subtotal() + $recargo;


        return ($this->subtotal() + $recargo)+ ($subtotalConRecargo*$this->iva)/100;

        
    }



    public function sumaEntregas()
    {
        $sumaEntregas = 0;

        foreach($this->entregas as $entrega)
        {
            $sumaEntregas += $entrega->entrega;
        }

        return $sumaEntregas;
    }
    

    public function controlEntregas()
    {        
        $sumaEntregas = $this->sumaEntregas();
        
        return ($this->totalVenta <= $sumaEntregas); 
        
    }

    public function saldoRestante()
    {

        return ( $this->total() - $this->entregaParcial );
    }

    public function saldoRestanteNuevo()
    {
        return ( $this->total() - $this->sumaEntregas());
    }

    public function devuelveEntrega(Entrega $entrega)
    {
        return ($this->total() + $entrega->entrega);
    }


}
