<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->address,
        'precio' => rand(1,100),
        'precioMayorista' => rand(1,100),
        'precioVidriero' => rand(1,100),
        'stock' => rand(1,100),
        'foto' => $faker->image,
        'categoria_id' => 1,
        'moneda' => 'Peso'


    ];
});
