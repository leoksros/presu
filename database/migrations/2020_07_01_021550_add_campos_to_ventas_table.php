<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCamposToVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ventas', function (Blueprint $table) {
            
            $table->float('adicional',8,2)->nullable();
            $table->float('valorDolar',8,2)->nullable();
            $table->float('recargo',8,2)->nullable();
            $table->float('iva',8,2)->nullable();
            $table->bigInteger('tipocliente_id')->unsigned();
            $table->foreign('tipocliente_id')->references('id')->on('tipoclientes');   
            $table->boolean('facturado');
            $table->bigInteger('cliente_id')->nullable()->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->string('telefono')->nullable();  

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ventas', function (Blueprint $table) {
            //
        });
    }
}
