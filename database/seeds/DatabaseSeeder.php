<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('monedas')->insert([
            'nombre' => 'Dolar',
            'valor' => 1            
        ]);

        DB::table('monedas')->insert([
            'nombre' => 'Peso',
            'valor' => 1            
        ]);

        DB::table('users')->insert([
            'name' => 'Esmerilados Rosario',
            'email' => 'esmeriladosrosario@gmail.com',
            'password' => Hash::make('HondaElite')
        ]);
            
        DB::table('categorias')->insert([
            'nombre' => 'General'
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Vidrios'
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Espejos'
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Tratamientos espejos'
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Tratamientos vidrios'
        ]);

        DB::table('tipoclientes')->insert([
            'nombre' => 'Vidrieros'
        ]);

        DB::table('tipoclientes')->insert([
            'nombre' => 'Consumidor final'
        ]);

        /* $this->call(ProductosSeeder::class); */
        
    }
}
