@extends('layouts.app')


@section('content')
    
@php
    /* dd($categorias); */
@endphp
                
<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">

            <div class="card-header text-center border"><h3>{{ __('Categorías') }}</h3>    </div>
            <br>


            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div class="row">
                <div class="col-sm-8">
  
                            @foreach ($categorias as $categoria)

                                <a class=" pr-1 pl-1  text-primary rounded-pill border" style="background-color: #ffffff;" href="{{route('productosCategoria',$categoria)}}">
                                {{$categoria->nombre}}
                                </a>
                            
                            @endforeach
   
                </div>
            </div>
    
            
            <ul class="list-group">
    
               
                
                @forelse ($categorias as $categoria)
                <br>

                     <li class="list-group-item">
                         <div class="d-flex row">
                             
    
                                  
                             
                             
                              

                            <div class="col-sm-10">   
                            <b>Nombre:</b> <a href="{{route('productosCategoria',$categoria)}}">{{$categoria->nombre}}</a>  
                            </div>
                        
                          


                             <div class="col-sm-2">
                                 <div class="row">
                                     
                                
                                     
                                    
                                     <div class="col"> 
                                        <a href="{{route('modificarCategoria',$categoria->id)}}">
                                            <button class="btn btn-warning"> 
                                                <i class="fas fa-pencil-alt"></i>	
                                            </button> 
                                         </a> 
                                     </div>

                                     <div class="col"> 
                                  
                                        <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('eliminarCategoria',$categoria)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"  type="submit">  
                                                <i class="fas fa-trash"></i>
                                            </button>                               
                                        </form>  
                                    </div>
                                 </div>
                             </div>

                            
                         </div>   
                         
                         
                     </li>

                @empty

                <div class="row d-flex justify-content-center mt-5 text-center">
                    <div class="col-sm-12">                            
                        <h3>No existen categorias guardadas.</h3>
                    </div>
                    <div class="col-sm-12">                            
                        <a class="text-primary" href="{{route('crearCategoria')}}">Click aquí para registrar categorias!</a>
                    </div>
                    
                </div>
                
                @endforelse
                
            </ul>
        </div>
    </div>
       
</div>



@endsection