@extends('layouts.app')


@section('content')
            
<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">

            <div class="card-header text-center border"><h3>{{ __($categoria->nombre) }}</h3>    </div>
            <br>

            <div class="row">

                <div class="col-sm-8">
  
                            @foreach ($categorias as $categoria)
                            
                           
                                <a class=" pr-1 pl-1  text-primary rounded-pill border" style="background-color: #ffffff;" href="{{route('productosCategoria',$categoria)}}">
                                {{$categoria->nombre}}
                                </a>
                            
                            @endforeach
   
                </div>
            </div>


           
            <ul class="list-group">
    
        
                
                @forelse ($productos as $producto)
                <br>
                     <li class="list-group-item">
                         <div class="d-flex row">
    
    
                            <div class="col-sm-2">
                                <img src="{{asset("/storage/$producto->foto")}}"  alt="" style="max-width: 150px" class="img-fluid align-middle ">
                            </div>                                
    
                             <div class="col-sm-1">
                               
                               <a href="{{route('producto',$producto->id)}}">{{ $producto->nombre }}</a> 
                             </div>


                             <div class="col-sm-2">
                                {{-- <b>Valor:</b> --}}  
                                @if($producto->moneda == "Dolar")
                                <b>CF: </b>u$s {{$producto->precio}}
                                <br>
                                <b>V: </b> u$s {{$producto->precioMayorista}}
                                <br>
                                <b>M: </b> u$s {{$producto->precioVidriero}}
                                
                                @else
                                <b>CF: </b>$ {{$producto->precio}}
                                <br>
                                <b>V:  </b> $ {{$producto->precioMayorista}}
                                <br>
                                <b>M: </b> $ {{$producto->precioVidriero}}
                                @endif
                                
                               
                                
                             </div>

                            <div class="col-sm-1">
                                <b>Stock:</b>  {{$producto->stock}}
                             </div>

                             <div class="col-sm-4">
    
                                <form method="POST" action="{{ route('agregarAPresupuesto', $producto)  }}">
                                    @csrf
                                    <div class="row">

                                        <div class="col-sm-4">

                                            <select class="form-control" name="tipoCliente" id="">
                                                <option value="cf">Consumidor final</option>
                                                <option value="m">Mayorista</option>
                                                <option value="v">Vidriero</option>
                                            </select>

                                        </div>

                                        <div class="col-sm-8">                                        
                                            <label for=""><b>Cantidad</b> </label> 
                                            <input type="number" step="any" min="1" name="cantidad" required class="col-sm-4 ">
                                            <button type="submit"  class="btn btn-success "> + </button>
                                        </div>
                                        
                                    </div>
                                    

                                </form>

                             </div>


    
                             <div class="col-sm-1">
                                 <div class="row d-flex justify-content-center">
                                     
                                     <div class="col-sm-6">
                                        <a href=" {{route('modificarProducto', $producto)}}">
                                            <button class="btn btn-warning" type="submit"> 
                                                <i class="fas fa-pencil-alt"></i>	
                                            </button> 
                                        </a>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('eliminarProducto', $producto)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">  
                                                <i class="fas fa-trash"></i>
                                            </button>                                      
                                        </form> 
                                    </div>
                                 </div>
                                
    
                                                                
                            </div>

                            
                            
                         </div>   
                         
                         
                     </li>
                @empty

                <div class="row d-flex justify-content-center mt-5 text-center">
    
                    <div class="col-sm-12">                            
                        <h3>Categoría sin productos.</h3>
                    </div>
                    
                    <br>
                    
                </div>

                
                @endforelse
                
            </ul>
            <br>
            {{ $productos->links()}}
        </div>
    </div>
       
</div>


@endsection