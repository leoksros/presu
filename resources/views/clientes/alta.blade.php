@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header text-center">{{ __('Registro de cliente') }}</div>
            
                            <div class="card-body">
                                <form method="POST" action="{{ route('guardarCliente') }}" enctype="multipart/form-data">
                                    @csrf
            
                                    <div class="form-group row">
                                        <label for="nombre" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Nombre cliente') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
            
                                            @error('nombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
            
                                    <div class="form-group row">
                                        <label for="cuit" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('CUIT') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="cuit" type="text" class="form-control @error('cuit') is-invalid @enderror" name="cuit" value="{{ old('cuit') }}"  autocomplete="cuit" autofocus>
            
                                            @error('cuit')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="direccion" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}"  autocomplete="direccion">
            
                                            @error('direccion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="telefono" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}"  autocomplete="telefono">
            
                                            @error('telefono')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

            
                                    <div class="form-group row">
                                        <label for="email" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('E-mail') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">
            
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                                                                                                                                                                          
                            
                                    <div class="form-group row">
                                        <label for="banco" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Banco') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="banco" type="text" class="form-control @error('banco') is-invalid @enderror" name="banco" value="{{ old('banco') }}"  autocomplete="banco" autofocus>
            
                                            @error('banco')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="cbu" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('CBU') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="cbu" type="text" class="form-control @error('cbu') is-invalid @enderror" name="cbu" value="{{ old('cbu') }}"  autocomplete="cbu" autofocus>
            
                                            @error('cbu')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="alias" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Alias') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="alias" type="text" class="form-control @error('alias') is-invalid @enderror" name="alias" value="{{ old('alias') }}"  autocomplete="alias" autofocus>
            
                                            @error('alias')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
            
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-success">
                                                {{ __('Agregar') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
       
</div>

@endsection