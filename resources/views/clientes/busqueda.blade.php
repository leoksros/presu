@extends('layouts.app')


@section('content')
    

@php
/*     dd($clientes,$presupuesto);
 */@endphp
                
<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">
            
            <div class="card-header text-center border"><h3>{{ __('Clientes') }}</h3>    </div>

            <br>
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

    
            <ul class="list-group">
    
               
                
                @forelse ($clientes as $cliente)
                <br>
            
                     <li class="list-group-item">
                         <div class="d-flex row">
                             
    
                             <div class="col">
                                  
                               ID: <a href="">{{$cliente->id}}</a> 
                             </div>                        

                            <div class="col">
                                Cliente: {{$cliente->nombre}}
                            </div>


                             <div class="col">
                                 Fecha: {{$cliente->created_at}}
                             </div>

                             <div class="col">
                                 <div class="row">

                                    <div class="col"> 

                                        <form method="POST" action="{{route('asignarCliente',compact('presupuesto','cliente'))}}" enctype="multipart/form-data">
                                            @csrf   
                                            @method('PUT')
                                            <button class="btn btn-success">
                                            <i class="fas fa-plus"></i>
                                        </form>
                                        
                                             
                                    </div>
                                     
                                     <div class="col"> 
                                            <button class="btn btn-primary">
                                                <a href="{{route('verCliente',$cliente)}}">
                                                <i class="fas fa-eye"></i>
                                            </a>  
                                     </div>

                                    
                                                

                                     

                                     <div class="col">
                                        <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('borrarCliente',$cliente)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"> 
                                                <i class="fas fa-trash"></i>
                                            </button>                             
                                        </form>
                         
                                    </div>
                                 </div>
                             </div>

                            
                         </div>   
                         
                         
                     </li>

                @empty

                <div class="row d-flex justify-content-center mt-5 text-center">                                             
                        <h3>No existen registros.</h3>
                </div>
                
                @endforelse
                
            </ul>
        </div>
    </div>
       
</div>



@endsection


