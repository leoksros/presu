@extends('layouts.app')


@section('content')
                
<div class="container">

    <div class="row d-flex justify-content-center">
    
        <div class="col">

            <div class="card-header text-center border"><h3>{{ __('Clientes') }}</h3>    </div>

            <br>
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div class="col">
                <form method="GET" action="{{ route('buscarCliente')}}" class="form-inline my-2 my-lg-0 " id="busquedaCliente">
                        @csrf
                        <input name="busqueda" class="form-control mr-sm-2" type="search" placeholder="Buscar cliente.." aria-label="Search">
                        <button class="btn btn-success my-2 my-sm-0"  form="busquedaCliente">
                        <i class="fas fa-search"></i>
                        </button>
                </form>
            </div>

        
        <div  class="rowVenta border shadow mt-3 px-3 py-3 row">

            <div class="col text-center">
               <b>Número</b>
            </div>

            <div class="col text-center">
              <b>Cliente</b>                                
            </div>
        
             <div class="col text-center">
                <b>Fecha</b>
             </div>

             <div class="col text-center"> 
                <b>Acciones</b> 
             </div>
             
        </div>

    
            <ul class="list-group">
    
               
                
                @forelse ($clientes as $cliente)
                <br>
            
                     <li class="list-group-item text-center">
                         <div class="d-flex row">
                             
    
                             <div class="col">
                                  
                              <b><a href="">{{$cliente->id}}</a> </b>
                             </div>                        

                            <div class="col">
                                {{ $cliente->nombre }}
                            </div>


                             <div class="col">
                               {{$cliente->created_at}}
                             </div>

                             <div class="col">
                                 <div class="row">
                                     
                                     <div class="col"> 
                                            <button class="btn btn-primary">
                                                <a href="{{route('verCliente',$cliente)}}">
                                                <i class="fas fa-eye"></i>
                                            </a>  
                                     </div>

                                     <div class="col"> 
                                        <a href="{{route('editarCliente',$cliente)}}"> 
                                           <button class="btn btn-warning"> 
                                            <i class="fas fa-pencil-alt"></i>		
                                           </button> 
                                       </a> 
                                   </div>
                                                

                                     

                                     <div class="col">
                                        <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('borrarCliente',$cliente)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"> 
                                                <i class="fas fa-trash"></i>
                                            </button>                             
                                        </form>
                         
                                    </div>
                                 </div>
                             </div>

                            
                         </div>   
                         
                         
                     </li>

                @empty

                <div class="row d-flex justify-content-center mt-5 text-center">                                             
                        <h3>No existen registros.</h3>
                </div>
                
                @endforelse
                
            </ul>
        </div>
    </div>
    <br>
    {{$clientes->links()}}
</div>

    

@endsection


