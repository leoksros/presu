@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>Notificaciones</h3> </div>

                <div class="card-body">
                  @if (session('status'))
                  <div class="alert alert-success text-center">
                      {{ session('status') }}
                  </div>
                @elseif(session('no'))
                        <div class="alert alert-danger text-center">
                            {{ session('no') }}
                        </div>
                @endif

                    <ul class="list-group h5">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                          Facturas pendientes de facturar
                          <span class="badge badge-danger badge-pill"><a href="{{route('facturacionPendiente')}}">{{$aFacturar}}</a>  </span>
                        </li>
                        <br>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                          Facturas con saldo deudor
                          <span class="badge badge-danger badge-pill"><a href="{{route('saldosPendientes')}}"> {{$cuentasConSaldoPendiente}}</a></span>
                        </li>
                        <br>
                        {{-- <li class="list-group-item d-flex justify-content-between align-items-center">
                          Otros recordatorios
                          <span class="badge badge-danger badge-pill">1</span>
                        </li> --}}
                      </ul>

                      
                </div>
            </div>

            <br><br>
            <div class="card">
               
              <div class="card-header text-center"><h3>{{ __('Cotización') }}</h3>    </div>
  
                  <div class="card-body">

                    @if (session('dolar'))
                  <div class="alert alert-success text-center">
                      {{ session('dolar') }}
                  </div>
               
                @endif
  
                          <div class="row align-items-center ">
  
  
                              <div class="col">
  
                                  <div class="col-md-auto ">
                                      <div class="card text-white bg-success mb-3 text-center" style="max-width: 18rem;">
                                          <div class="card-header "> <h5 class="card-title ">Dolar</h5></div>
                                          <div class="card-body">            
                                          <p class="card-text"> <h1> {{$moneda->valor}} </h1> </p>
                                          </div>
                                      </div>
                                  </div>  
                              </div>
  
                              <div class="col text-center">
                                  
                                  <form method="POST"  action="{{route('actualizarValor',$moneda)}}" enctype="multipart/form-data">
                              
                                      @csrf  
                                      @method('PUT')
                                      
  
                                      <div class="form-group row ">
  
                                          <div class="col text-center">
                                              <h5 class="card-title">Nuevo valor</h5> 
                                          </div>
                                         
                      
                                          <div class="col-md-11">
  
                                              
                                              <input id="valor" type="text" class="form-control @error('valor') is-invalid @enderror" name="valor" value="{{ old('valor') }}" required autocomplete="valor" autofocus>
                      
                                              @error('valor')
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                                          </div>
                                      </div>  
                      
                                      
                      
                                      <div class="form-group row mb-0 text-center">
                                          <div class=" offset-md-4 ">
                                              <button type="submit" class="btn btn-success">
                                                  {{ __('Actualizar') }}
                                              </button>
                                          </div>
                                      </div>          
                                  </form>
  
                              </div>
  
                              
                              
                          </div>
               
  
  
                  </div>
          </div>

        </div>
    </div>
</div>
@endsection
