<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'eR') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>


    <!-- Font Awesome JS Sidebar -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    {{-- Iconos --}}

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>

@php

    if(Cart::count() == 0)

    {
            $boton = false;
    }
    else {
            $boton = true;
    }

@endphp


<body>

    @auth

        
        <div class="wrapper">
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3>esmerilados Rosario</h3>
                </div>

                <ul class="list-unstyled components">
                    <p>Menú</p>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}"><i class="fas fa-home"></i> Inicio</a>
                    </li>
                        
                    <li>
                        <a href="#pageCategorias" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> <i class="fas fa-sitemap"></i> Categorías</a>
                        <ul class="collapse list-unstyled" id="pageCategorias">

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('crearCategoria') }}">{{ __('Crear categoría') }}</a>
                            </li>  

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('listaCategorias') }}">{{ __('Lista de categorias') }}</a>
                            </li>

                                                                

                        </ul>
                    </li>


                    <li>
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-box-open"></i> Productos</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">                                       
                            
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('formularioProducto') }}">{{ __('Incorporar producto') }}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('listaProductos') }}">{{ __('Lista de productos') }}</a>
                                </li>

                                

                        </ul>
                    </li>

                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-file-alt"></i> Presupuestos</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('presupuesto') }}">{{ __('Nuevo presupuesto') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('listaPresupuestos') }}">{{ __('Presupuestos guardados') }}</a>
                            </li>
                            

                            

                        </ul>
                    </li>

                    <li>

                        <a href="#pagSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-dollar-sign"></i>  Ventas</a>

                        <ul class="collapse list-unstyled" id="pagSubmenu">

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('listaVentas') }}">{{ __('Lista') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('saldosPendientes')}}">{{ __('Saldos pendientes') }}</a>
                            </li>
                        
                        </ul>

                    </li>

                    {{-- Crear clase clientes y atributos --}}
                    
                    <li>

                        <a href="#config" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-user-friends"></i> Clientes</a>

                        <ul class="collapse list-unstyled" id="config">

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('crearCliente')}}">{{ __('Crear') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('listaClientes')}}">{{ __('Lista clientes') }}</a>
                            </li>
                                                   
                        </ul>

                    </li>
                </ul>

            </nav>

            <!-- Page Content  -->
            <div id="content">

                <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #51535a;">
                    <div class="container-fluid">

                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                            <i class="fas fa-align-left"></i>
                            <span> </span>
                        </button>
                        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fas fa-align-justify"></i>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="nav navbar-nav ml-auto">

                                @auth

                                @if($boton)
                                <li class="nav-item">
                                    <a href="{{ route('presupuesto') }}" class="nav-link">{{ __('Presupuesto en proceso ') }}<i class="fas fa-file-alt"></i></a>
                                </li>
                                @endif

                                <li class="nav-item">
                                    <a class="dropdown-item text-white" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                {{ __('Salir') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                </li>

                                @endauth
                            </ul>
                        </div>
                    </div>
                </nav>

                @yield('content')


                </div>
        </div>
        
        <!-- jQuery CDN - Slim version (=without AJAX) -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

        {{--  <!-- Popper.JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <!-- Bootstrap JS -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        --}}
    
        <script type="text/javascript">
            $(document).ready(function () {
                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                });
            });

            $("button").click(function() {
                    
                const data = JSON.parse(this.id);
                let idVenta = data['id'];
                const url = '../obtenerventa/'+idVenta;
                $.get(url, function(data, status) {
                    console.log(`${data}`);
                });

            });

        </script>

        @endauth

        @guest
            @yield('content')
        @endguest
        
</body>

</html>