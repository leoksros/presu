@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header text-center">{{ __('Registro de artículo') }}</div>
            
                            <div class="card-body">
                                <form method="POST" action="{{ route('registrarProducto') }}" enctype="multipart/form-data">
                                    @csrf
            
                                    <div class="form-group row">
                                        <label for="nombre" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Nombre cliente') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
            
                                            @error('nombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
            
                                    <div class="form-group row">
                                        <label for="descripcion" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion" autofocus>
            
                                            @error('descripcion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="total" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Total') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="total" type="number" class="form-control @error('total') is-invalid @enderror" name="total" value="{{ old('total') }}" required autocomplete="total">
            
                                            @error('total')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

            
                                    <div class="form-group row">
                                        <label for="stock" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Stock') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="stock" type="number" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock') }}" required autocomplete="stock">
            
                                            @error('stock')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                                                                                                                                                                          
                            
            
                                    <div class="form-group row">
                                        <label for="productoimagen" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Cargar imágen') }}</label>
            
                                        <div class="col-md-6">
                                            <input id="productoimagen" type="file" class="form-control @error('productoimagen') is-invalid @enderror" name="productoimagen" value="{{ old('productoimagen') }}" required autocomplete="productoimagen" autofocus>
            
                                            @error('productoimagen')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
            
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-success">
                                                {{ __('Agregar') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
       
</div>

@endsection