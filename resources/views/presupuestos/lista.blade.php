@extends('layouts.app')


@section('content')
    


                
<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">

            <div class="card-header text-center border"><h3>{{ __('Presupuestos') }}</h3>    </div>

            <br>
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div  class="rowVenta border shadow mt-3 px-3 py-3 row">

                <div class="col text-center">
                   <b>Número</b>
                </div>

                <div class="col text-center">
                  <b>Cliente</b>                                
                </div>
            
                 <div class="col text-center">
                    <b>Fecha</b>
                 </div>

                 <div class="col text-center"> 
                    <b>Acciones</b> 
                 </div>
                 
            </div>

            
    
            
            <ul class="list-group">
    
               
                
                @forelse ($presupuestos as $presupuesto)
                <br>
             
                     <li class="list-group-item text-center">
                         <div class="d-flex row">                             
    
                             <div class="col">
                                <b> <a href="{{route('verPresupuesto',$presupuesto->id)}}">{{$presupuesto->id}}</a>        </b>
                               <a href="{{-- {{route('producto',$producto->id)}} --}}">{{-- {{ $producto->nombre }} --}}</a> 
                             </div>                        

                            <div class="col">
                                @if($presupuesto->cliente_id != null)                                
                                  <a class="stretched-link text-primary" href="{{route('verCliente',$presupuesto->cliente)}}">{{$presupuesto->cliente->nombre}}</a>
                                    
                                @else
                                    {{$presupuesto->nombreCliente}}
                                @endif
                            </div>                                                    
                             
                             <div class="col">
                                {{$presupuesto->created_at}}
                             </div>

                             <div class="col">
                                 <div class="row">
                                     
                                     <div class="col"> 
                                         <a href="{{route('verPresupuesto',$presupuesto->id)}}">
                                            <button class="btn btn-primary">
                                                <i class="fas fa-eye"></i>
                                            </a>  
                                     </div>

                                     <div class="col"> 
                                         <a target="_blank" href="{{route('pdfPresupuesto',$presupuesto->id)}}"> 
                                            <button class="btn btn-success"> 
                                                <i class="fas fa-arrow-down"></i>	
                                            </button> 
                                        </a> 
                                    </div>
                                    
                                     <div class="col"> 
                                        <a href="{{route('verPresupuesto',$presupuesto->id)}}">
                                            <button class="btn btn-warning"> 
                                                <i class="fas fa-pencil-alt"></i>	
                                            </button> 
                                         </a> 
                                     </div>

                                     <div class="col"> 
                                         <a onclick="return confirm('Está seguro de eliminar?')" href="{{route('eliminarPresupuesto', $presupuesto->id)}}">
                                            <button class="btn btn-danger"> 
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a> 
                                    </div>
                                 </div>
                             </div>

                            
                         </div>   
                         
                         
                     </li>

                @empty

                <div class="row d-flex justify-content-center mt-5">
    
                    <h3>No existen presupuestos guardados.</h3>
                </div>
                @endforelse
                
            </ul>
        </div>
    </div>
       
</div>



@endsection