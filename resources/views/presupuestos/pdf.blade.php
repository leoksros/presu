<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>Presupuesto N°: {{$presupuesto->id}}</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .text-right {
            text-align: right;
        }
    </style>

</head>
<body class="login-page" style="background: white">

    <div>
        <div class="row">
            <div class="col-xs-7">
                <h4>De:</h4>
                <strong>Esmerilados Rosario</strong><br>    
                Juan B. Justo 1577  <br>
                Rosario, Santa Fe<br>
                Tel: 0341 435-0828 <br>
                E-Mail: esmeriladosrosario@gmail.com <br>

                <br>
            </div>

            <div class="col-xs-4">                
                <img src="https://i.ibb.co/Ybmtzhx/Logo75x250.png" alt="Logo75x250">
            </div>
        </div>

        <div style="margin-bottom: 0px">&nbsp;</div>

        <div class="row">
            <div class="col-xs-6">
                <h4>Para:</h4>
                <address>
                    @if($presupuesto->cliente_id == null)
                        <strong>{{$presupuesto->nombreCliente}}</strong><br>
                        <span>{{$presupuesto->email}}</span> <br>     
                        <span>{{$presupuesto->descripcion}}</span> <br>
                    @else
                        <strong>{{$presupuesto->cliente->nombre}}</strong><br>
                        <span>{{$presupuesto->cliente->email}}</span> <br>     
                        <span>{{$presupuesto->cliente->telefono}}</span> <br>                    
                    @endif
                </address>
            </div>

            <div class="col-xs-5">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <th>Número presupuesto:</th>
                            <td class="text-right">{{$presupuesto->id}}</td>
                        </tr>
                        <tr>
                            <th> Fecha: </th>
                            <td class="text-right">{{$presupuesto->created_at}}</td>
                        </tr>
                    </tbody>
                </table>

                <div style="margin-bottom: 0px">&nbsp;</div>

                <table style="width: 100%; margin-bottom: 20px">
                    <tbody>
                        <tr class="well" style="padding: 5px">
                            <th style="padding: 5px"><div> Total presupuesto: </div></th>
                            <td style="padding: 5px" class="text-right"><strong> {{$presupuesto->total()}} </strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <table class="table">
            <thead style="background: #F5F5F5;">
                <tr>
                    <th>Lista de productos</th>
                    <th></th>
                    @if($presupuesto->tipocliente->nombre == 'Consumidor final')
                            <th class="text-right"></th>
                             
                            @else
                            <th class="text-right">Precio</th>
                    @endif
                    
                </tr>
            </thead>
            <tbody>

                @foreach ($presupuesto->items as $items => $item)

                    
                    <tr>                     
                            <td>
                               
                                <div class="row"> 
                                    <div class="col-xs-2">
                                        <img src="{{public_path('/storage/'.$item->producto->foto)}}" style="width: 75px" alt="Logo75x75">
                                    </div>
                                    <div class="col-xs-4">
                                        <strong>{{$item->producto->nombre}}</strong>
                                        <p>Código: {{$item->producto->codigoProducto}}</p>
                                        <p>{{$item->producto->descripcion}}</p>
                                        
                                        @if($presupuesto->tipocliente->nombre == 'Consumidor final')
                                                 
                                                 
                                                @else
                                                <p>Cantidad: {{$item->cantidad}}</p>
                                                <p>Precio unidad: 
                                                    @if($item->producto->moneda == 'Dolar')
                                                    u$s
                                                    @else
                                                    $
                                                    @endif                                                                                                        
                                                    {{$item->producto->precio}}</p>
                                        @endif
                                        
                                    </div>
                                </div>  

                            </td>
                            
                            <td></td>
                           
                            @if($presupuesto->tipocliente->nombre == 'Consumidor final')
                            <td class="text-right"></td>
                             
                            @else
                            <td class="text-right">${{$item->cantidad * $item->precio}}</td>
                            @endif


                    </tr>

                @endforeach
     
            </tbody>

        </table>

            

            <div style="margin-bottom: 0px">&nbsp;</div>

            <div class="row">
                <div class="col-xs-8 invbody-terms">
                    {{-- Muchas gracias por su consulta. <br>
                    <br>
                    <h4>Términos y condiciones</h4>
                    <p>El presupuesto tendrá una fecha de vencimiento: xx/xx/xxxx</p> --}}
                </div>
            </div>
        </div>


        <div style="margin-bottom: 0px">&nbsp;</div>

            <div class="row">
                <div class="col-xs-8 invbody-terms">
                    Muchas gracias por su consulta. <br>
                    <br>
                    <h4>Términos y condiciones</h4>
                    <p>El presupuesto tendrá vigencia válida hasta 72 hs hábiles luego de su confección.</p>
                    <p>Valor dolar: u$s {{$presupuesto->valorDolar}}. Fecha: {{$presupuesto->created_at}}</p>
                </div>
            </div>


        <div class="row">
            <div class="col-xs-6"></div>
            <div class="col-xs-5">
                <table style="width: 100%">
                    <tbody>
                        <tr class="well" style="padding: 5px">
                            <th style="padding: 5px"><div> Total: </div></th>
                            <td style="padding: 5px" class="text-right"><strong> ${{$presupuesto->total()}} </strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </body>
    </html>