@extends('layouts.app')


@section('content')


@php        


        if(Cart::count() == 0)

        {
                $boton = false;
        }
        else {
                $boton = true;
        }

        $subtotal = 0;
    
         

  
@endphp

<div class="container">
    
        <div class="card-header text-center border"><h3 class="font-weight-bold mb-4">Presupuesto</h3>    </div>

        


    <br>

    @if (session('status'))
            <div class="alert alert-success text-center">
                 {{ session('status') }}
            </div>
    @endif

    <div class="row">

        
            
            <div class="col col-md-7">

                

                       {{-- @if($boton) --}}
                                <div class="card w-100 mb-4">
                                        <div class="card-body">

                                                <div class="mb-3 card-header border"> 
                
                                                        <h4 class="font-weight-bold mb-4">Datos</h4>
                                               
                                        </div>        
                                          
                                                
                                          
                                                        <form action="{{route('generarPresupuesto' )}}" id="formulario">
                                          

                                                        @csrf

                                                       

                                                                <label for="">Cliente:</label>        
                                                                <input class="form-control" name="nombreCliente" type="text">
                                                                <br>
                                                                <label for="">Email:</label>
                                                                <input class="form-control"  name="email" type="text">
                                                                <br>     
                                                                <label for="">Aclaracion:</label>
                                                                <input class="form-control"  name="descripcion" type="text" >         
                                                                
                                                                
                                                                <br>
                                                                <div>
                                                        
                                                

                                                        <br>

                                                        <div class="">

                                                                <div class="mb-3 card-header border"> 
                
                                                                                <h4 class="font-weight-bold mb-4">Ajustes ($):</h4>
                                                                       
                                                                </div>
                
                                                                <div class="row d-flex">
                
                                                                        <div class="col-sm-6">
                                                                                <label for="">Recargo (%):</label>
                                                                                <input class="form-control"  name="recargo" type="number" step="any" min="0" value="0">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                                <label for="">IVA (%):</label>
                                                                                <input class="form-control"  name="iva" type="number" step="any" min="0" value="0">
                                                                        </div>
                                                                        
                                                                        <div class="col-sm-6">
                                                                                <label for="">Adicional ($):</label>
                                                                                <input class="form-control"  name="adicional" type="number" step="any" min="0" value="0">
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                                <label for="">Tipo cliente:</label>
                                                                                <select class="custom-select" id="inputGroupSelect01" name="tipoCliente">                                                                                        

                                                                                        @foreach ($tiposcliente as $tipo)
                                                                                         <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>                                                                                
                                                                                        @endforeach          
                                                                                             
                                                                                      </select>
                                                                        </div>
                                                                </div>
                
                                                        </div>
                                                        
                                                        <br>
                                                       
                                                        </div>
                                                        
                                                </form>
                                        </div>
                                </div>
                       {{--  @endif --}}

                        <div>
                        </div>        
                        {{-- @if($boton) --}}
                        <div class="row d-flex justify-content-around card-header border">
                                <div class="col">
                                        <h3 class="font-weight-bold mb-4">
                                                Productos                                               
                                        </h3> 
                                        
                                </div>
                                <div class="col">
                                        <a  class="btn btn-primary" href="{{route('listaProductos')}}">
                                                Agregar <i class="fas fa-plus"></i>
                                        </a>
                                </div>

                                <div class="col">                                      
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Nuevo <i class="fas fa-plus"></i></button>                                        
                               </div>
                                
                        </div>

                        <br>
                        {{-- @endif --}}

                    @forelse ($presupuesto as $item)
                    
                                
                                
                            <div class="card w-100 mb-4">
                   
                                    <div class="card-body">
                                            <h5 class="card-title ">
                                                    <a class="tituloArticulo" {{-- href="{{url('producto')."/$item->id"}}" --}}>{{$item->name}}</a>
                                            </h5>
                                            
                                            <div class="row">

                                                    <div class="col col-sm-4">                                                                                 
                                                        <img src="{{asset("/storage/".$item->options->foto)}}" class="img-fluid tamanioArticulo">                                                                                                                                                                                    
                                                    </div>

                                                    <div class="col col-sm-8">
                                                            <p class="card-text"> {{$item->options->descripcion}}</p>
                                                            <p>  

                                                                <form action="{{route('modificarItem',$item->rowId)}}" >

                                                                        @csrf
                                                                        <div class="row">
                                                                                <div class="col-sm-5"> <b>Cantidad:</b></div>
                                                                                <div class="col-sm-4">
                                                                                        <input class="form-control" type="number" step="any" min="0" name="cantidad" value="{{$item->qty}}"></div>
                                                                                <div class="col-sm-2"><button class="btn btn-warning" type="submit"> 
                                                                                        <i class="fas fa-sync-alt"></i>	
                                                                                    </button> </div>
                                                                        </div>
                                                                       
                                                                        
                                                                        
                                                                        
                                                                </form>
                                                                                                                                   
                                                            </p>
                                                            
                                                            @if($item->options->moneda == 'Dolar')  
                                                                        <p> 
                                                                        <b>Precio:</b> u$s {{$item->price}}
                                                                        </p>
                                                                        <p> 
                                                                        <b>Suma:</b> {{ $item->price*$item->qty* $dolar->valor}}
                                                                        </p>
                                                                @else
                                                                        <p> 
                                                                                <b>Precio:</b> {{$item->price}}
                                                                        </p>
                                                                        <p> 
                                                                                <b>Suma:</b> {{ $item->price*$item->qty}}
                                                                        </p>
                                                             @endif 
                                                       
                                                            


                                                            <div class="row">

                                                          

                                                                <div class="col-md-3 ml-md-auto">                                                                   
                                                                    <a onclick="return confirm('Está seguro de quitar el producto del presupuesto?')" class="btn btn-danger" href="{{route('removerItem',$item->rowId)}}"> 
                                                                        <i class="fas fa-trash"></i>
                                                                    </a>                                                                    
                                                                </div>

                                                            </div>

                               
                                                    </div>
                                            </div>
                                    
                    
                                    </div>
                            </div>
                    @empty
                    
                    
                           
                                
                          
                        
                  

                
                    
                    @endforelse

                    

            </div>
    
            {{-- @if($boton) --}}
            <div class="col-sm-12 col-md-4 color-marron-letra ">
                    <div class="card" style="width: 18rem;">
                            <div class="card-body  fondoRosa">
                            <h4 class="font-weight-bold mb-4">Pedido</h5>

                                    <div class="row d-flex">
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                                            
                                                            <span class=" font-weight-bold">Subtotal: </span>
                                                            <br><br>
                                                              
                                                            {{-- <span class=" font-weight-bold">Total:</span> --}}
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 ">

                                                                @foreach ($presupuesto as $item)
                                                                    @if($item->options->moneda == 'Dolar')
                                                                        @php
                                                                         $subtotal = $subtotal + $item->price*$item->qty*$dolar->valor;    
                                                                        @endphp
                                                                         
                                                                    @else
                                                                        
                                                                            @php
                                                                                $subtotal = $subtotal + $item->price*$item->qty;
                                                                            @endphp  
                                                                        
                                                                    @endif    
                                                                @endforeach

                                                            <span class="">$ {{$subtotal}} {{-- {{Cart::subtotal()}} --}}

                                                                


                                                            </span>
                                                            <br><br>
                                                  
                                                            {{-- <span class="">$ A calcular{{Cart::subtotal()}}</span> --}}
                                            </div>

                                    </div>
                                    <br>
                                   
                        @if($boton)    
                           <button type="submit" class="btn btn-success btn-lg btn-block " form="formulario"> 
                               Guardar presupuesto
                               
                           </button> 
                        @endif   

                            </div>
                    </div>
                

            </div>
           {{--  @endif --}}

    </div>


</div>

{{-- modal --}}

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Nuevo item</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="{{route('agregarAPresupuestoProductoProvisorio')}}" id="formularioProductoProvisorio">
                      
                @csrf
                      
      
                <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nombre:</label>
                        <input type="text" required class="form-control" name="nombre" id="recipient-name">
          </div>        

          <div class="form-group">
            <label for="recipient-name"  class="col-form-label">Precio:</label>
            <input type="number" required min="1" class="form-control" name="precio" id="recipient-name">
          </div>

          <div class="form-group">
                <label for="recipient-name"  class="col-form-label">Cantidad:</label>
                <input type="number" required min="1" class="form-control" name="cantidad" id="recipient-name">
              </div>

          <div class="form-group">
            <label for="message-text"  class="col-form-label">Descripcion:</label>
            <textarea class="form-control" required name="descripcion" id="message-text"></textarea>
          </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" form="formularioProductoProvisorio" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </div>
      </div>
      
      {{-- modal --}}


@endsection