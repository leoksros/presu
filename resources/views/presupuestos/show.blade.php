@extends('layouts.app')


@section('content')

<div class="container">
    
        <div class="card-header border">
                <h4 class="h3  font-weight-bold mb-4">Presupuesto
                        <a  onclick="return confirm('Confirmar venta.')" class="btn btn-success" href="{{route('registrarVenta', $cliente)}}">
                                Confirmar venta <i class="fas fa-check"></i>
                        </a>
                
                </h4>
        </div>
    

    <br>
    <br>

    @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
    @elseif(session('no'))
                    <div class="alert alert-warning text-center">
                        {{ session('no') }}
                    </div>
    @elseif(session('error'))
        <div class="alert alert-danger text-center">
                {{ session('error') }}
        </div>
    @endif

    <div class="row">
            
            <div class="col col-md-7">

                <div class="card w-100 mb-4">
                        <div class="card-body">

                                <div class="card-header border">
                                        <div class="row">
                                                <div class="col-4 col-xs-6"> <h4 class="h3  font-weight-bold mb-4">Datos </h4>
                                                </div>
                                                <div class="col-8 col-xs-6">
                                                        <form method="GET" action="{{ route('buscarCliente',$cliente)}}" class="form-inline my-2 my-lg-0 " id="busquedaCliente">
                                                                @csrf
                                                                <input name="busqueda" class="form-control mr-sm-2" type="search" placeholder="Buscar cliente.." aria-label="Search">
                                                                <button class="btn btn-success my-2 my-sm-0"  form="busquedaCliente">
                                                                <i class="fas fa-search"></i>
                                                                </button>
                                                        </form>
                                                </div>
                                        </div>
                     
                                </div>

                                
                                
                                <form method="POST" action="{{route('actualizarPresupuesto',$cliente->id /* cliente = presupuesto */ )}}" id="formulario">

                                        @csrf
                                        
                                        
                                        

                                        @if($cliente->cliente == null)
                                
                                                <br>
                                                <label for="">Cliente: </label><input class="form-control" value="{{$cliente->nombreCliente}}" name="nombreCliente" type="text">
                                                <br>
                                                <label for="">Email:</label><input class="form-control" value="{{$cliente->email}}" name="email" type="text">
                                                <br>
                                                <label for="">Aclaracion:</label><input class="form-control" value="{{$cliente->descripcion  }}" name="descripcion" type="text">
                                        @else 
                                        <div class="row mt-3">
                                                        
                                                <div class="col-sm-6">

                                                        <div class="row d-flex">
                                                                <div class="col-sm-9">
                                                                        <label for="">Cliente:</label>        
                                                                      <input readonly class="form-control" name="nombreCliente" type="text" value="{{$cliente->cliente->nombre}}">
                                                                </div>
                                                                <div class="col-sm-1">
                                                                        <label for=""> </label>   
                                                                        <a onclick="return confirm('Está seguro de quitar el cliente del presupuesto?')" class="btn btn-danger" href="{{route('quitarCliente',$cliente)}}"><i class="fas fa-user-times"></i></a>                                                                        
                                                                </div>
                                                               
                                                        </div>
                                                                                                                 
                                                </div>

                                                <div class="col-sm-6">
                                                        <label for="">CUIT:</label><input class="form-control"  readonly  name="cuit" type="text" value="{{$cliente->cliente->cuit}}">
                                                </div>
                                                
                                                <div class="col-sm-6">
                                                        <label for="">Direccion:</label><input class="form-control"  readonly  name="direccion" type="text" value="{{$cliente->cliente->direccion}}">
                                                </div>

                                                <div class="col-sm-6">
                                                        <label for="">Banco:</label>        
                                                        <input class="form-control" readonly  name="nombreCliente" type="text" value="{{$cliente->cliente->banco}}">                                                          
                                                </div>
                                                
                                                
                                                <div class="col-sm-6">
                                                        <label for="">Email:</label><input class="form-control"  readonly name="email" type="text" value="{{$cliente->cliente->email}}">
                                                </div>
                                                
                                                
                                                <div class="col-sm-6">
                                                        <label for="">CBU:</label><input class="form-control" readonly name="cbu" type="text" value="{{$cliente->cliente->cbu}}">

                                                </div>
                                                <div class="col-sm-6">
                                                        <label for="">Aclaracion:</label><input class="form-control" readonly name="descripcion" type="text" value="{{$cliente->cliente->descripcion}}">

                                                </div>
                                                <div class="col-sm-6">
                                                        <label for="">Alias:</label><input class="form-control" readonly name="alias" type="text" value="{{$cliente->cliente->alias}}">

                                                </div>
                                               
                                        </div>
                                        @endif

                                        <br>

                                        <div class="">

                                                <div class="mb-3 card-header border"> 

                                                                <h4>Ajustes ($):</h4>
                                                       
                                                </div>

                                                <div class="row d-flex">

                                                        <div class="col-sm-6 mb-1">
                                                                <label for="">Recargo (%):</label>
                                                                <input class="form-control"  name="recargo" type="number" step="any" min="0" value="{{$cliente->recargo}}">
                                                        </div>
                                                        <div class="col-sm-6 mb-1">
                                                                <label for="">IVA (%):</label>
                                                                <input class="form-control"  name="iva" type="number" step="any" min="0" value="{{$cliente->iva}}">
                                                        </div>
                                                        <div class="col-sm-6 mb-1">
                                                                <label for="">Adicional ($):</label>
                                                                <input class="form-control"  name="adicional" type="number" step="any" min="0" value="{{$cliente->adicional}}">
                                                        </div>

                                                        <div class="col-sm-6 mb-1">
                                                                <label for="">Tipo cliente:</label>
                                                                <select class="custom-select" id="inputGroupSelect01" name="tipoCliente">                                                                                        

                                                                        @foreach ($tiposcliente as $tipo)
                                                                                @if ($tipo->nombre == $cliente->tipocliente->nombre)
                                                                                 <option selected value="{{$tipo->id}}">{{$tipo->nombre}}</option> 
                                                                                 @else
                                                                                 <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>     
                                                                                @endif                                                                                                                                                      
                                                                        @endforeach          
                                                                             
                                                                      </select>
                                                        </div>

                                                        <div class="col-sm-6">
                                                                        <label for="">Valor dolar asignado:</label>
                                                                        <input class="form-control"  name="valorDolar" type="number" step="any" min="1" value="{{$cliente->valorDolar}}">
                                                        </div>
                                                </div>

                                        </div>

                                        <br><br>
                                </form>
                                        <div class="card-header border">

                                                <h4>
                                                        Productos: 
                                                        <a  class="btn btn-primary" href="{{route('listaProductosPresupuestoGuardado', $cliente)}}">
                                                                Agregar <i class="fas fa-plus"></i>
                                                        </a>

                                                                                           
                                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Nuevo <i class="fas fa-plus"></i></button>                                        
                                                       

                                                </h4>    
                                        </div>
                                        <br>
                                        
                        
                                   

                                @forelse ($presupuesto as $item)
                        
                                <div class="card w-100 mb-4">
                                        
                                        <div class="card-body">
                                                <h5 class="card-title ">
                                                        <a class="tituloArticulo" href="{{route('producto', $item->producto)}}">{{$item->producto->nombre}} // Cod: {{$item->producto->codigoProducto}}</a>
                                                        
                                                </h5>
                                                
                                                <div class="row ">

                                                        <div class="col col-sm-4">                                                                                 
                                                                <img src="{{asset("/storage/".$item->producto->foto)}}" class="img-fluid tamanioArticulo">                                                                                                                                                                                    
                                                        </div>

                                                        <div class="col col-sm-8">        
                                                                <form method="GET" action="{{ route('updateItemPresupuesto',$item)}}" class="form-inline my-2 my-lg-0 " >
                                                                        @csrf
                                                                        
                                                                       
                                                                        <div class="col-sm-12 mb-2">
                                                                                <p class="card-text"> {{$item->producto->descripcion}}</p>
                                                                        </div>
                                                                        
                                                                        <div class="col-sm-12">
                                                                             
                                                                                <p> <b>Cantidad:            </b> 
                                                                                
                                                                                <input step="any" type="number" class="form-control" name="cantidad" value="{{$item->cantidad}}"></p>
                                                                        </div>
                                                                        
                                                                     

                                                                        @if($item->producto->moneda == 'Dolar')
                                                                        <div class="col-sm-12">
                                                                                <p> <b>Precio unit (u$s):</b> 
                                                                                        <input step="any" type="number" readonly class="form-control" name="precio" value="{{$item->producto->precio}}"></p>
                                                                        </div>
                                                                        @endif
                                                                        <div class="col-sm-12">
                                                                                <p> <b>Precio unit ($):    </b> 
                                                                                        <input step="any" type="number" readonly class="form-control" name="precio" value="{{$item->precio}}"></p>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                                <p>                                                         
                                                                                        <b>Suma:</b>
                                                                                
                                                                                        @php 
                                                                                                echo( $item->precio*$item->cantidad); 
                                                                                        @endphp 
                                                                                </p>
                                                                        </div>
                                                                                
                                                                       
                                                                        <div class="container">

                                                                                <div class="row justify-content-end">
                                                                                       
                                                                                        <div class="col-4">
                                                                                                <button class="btn btn-warning" type="submit" > 
                                                                                                        <i class="fas fa-sync-alt"></i>
                                                                                                </button>  
                                                                                        </div>                                                                                                                                                                                                                
                                                                                        
                                                                                        <div class="col-4">
                                                                                                <a onclick="return confirm('Está seguro de quitar el producto del presupuesto?')" class="btn btn-danger" href="{{route('removerItemPresupuesto',$item)}}"> <i class="fas fa-trash"></i> </a>                                                                                                                                                          
                                                                                        </div>
                                                                                                                                                                                                
                                                                                                                                                                                
                                                                                </div>                    
                                                                        </div>
                                                                                                                                                                
                                                                                   
                                                                </form>   
                                                        </div>             
                                                </div>
                                        
                        
                                        </div>
                                                 
                                </div>
                              
                                @empty                        
                                        <div class="row d-flex justify-content-center">
                                                <h3 class="text-danger">Presupuesto vacío.</h3>
                                                
                                                <p> <a href="{{route('eliminarPresupuesto', $cliente->id)}}"> Borrar</a> </p>
                                        </div>
                                @endforelse
                   





                                
                        </div>

                </div>


                    

            </div>
    
            <div class="col-sm-12 col-md-4 color-marron-letra ">
                    <div class="card" style="width: 18rem;">
                            <div class="card-body  fondoRosa">
                            <h4 class="h3  font-weight-bold mb-4">Pedido</h5>

                                    <div class="row d-flex">
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                                            
                                                            <span class=" font-weight-bold">Subtotal: </span>
                                                            <br><br>
                                                              
                                                            <span class=" font-weight-bold">Total:</span>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 ">
                                                            <span class="">${{$cliente->subtotal()}}</span>
                                                            <br><br>
                                                  
                                                            <span class="">$ {{$cliente->total()}}</span>
                                            </div>

                                    </div>
                                    <br>
                                   
                                    
                           <button onclick="return confirm('Confirmar cambios.')" type="submit" class="btn btn-primary btn-lg btn-block " form="formulario"> 
                               Registrar cambios
                           </button> 

                            </div>
                    </div>
                

            </div>

    </div>


</div>

{{-- modal --}}

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Nuevo item</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="{{route('actualizarPresupuestoGuardado',$cliente->id)}}" id="formularioProductoProvisorio">
                      
                @csrf
                <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nombre:</label>
                        <input type="text" required class="form-control" name="nombre" id="recipient-name">
          </div>        

          <div class="form-group">
            <label for="recipient-name"  class="col-form-label">Precio:</label>
            <input type="number" required min="1" class="form-control" name="precio" id="recipient-name">
          </div>

          <div class="form-group">
                <label for="recipient-name"  class="col-form-label">Cantidad:</label>
                <input type="number" required min="1" class="form-control" name="cantidad" id="recipient-name">
              </div>

          <div class="form-group">
            <label for="message-text"  class="col-form-label">Descripcion:</label>
            <textarea class="form-control" required name="descripcion" id="message-text"></textarea>
          </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" form="formularioProductoProvisorio" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </div>
      </div>
      
      {{-- modal --}}


@endsection