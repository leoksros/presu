@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header text-center"><h3>{{ __('Registro de producto') }}</h3></div>
            
                            <div class="card-body">
                                <form method="POST" action="{{ route('registrarProducto') }}" enctype="multipart/form-data">
                                    @csrf
            
                                    <div class="form-group row">
                                        <label for="nombre" class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Nombre') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
            
                                            @error('nombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="codigoProducto" class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Código') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="codigoProducto" type="text" class="form-control @error('codigoProducto') is-invalid @enderror" name="codigoProducto" value="{{ old('codigoProducto') }}"  autocomplete="codigoProducto" autofocus>
            
                                            @error('codigoProducto')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    
            
                                    <div class="form-group row">
                                        <label for="descripcion" class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Descripcion') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="-  "  autocomplete="descripcion" autofocus>
            
                                            @error('descripcion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="precio"  class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Precio c. final') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="precio" type="number" step="0.01" class="form-control @error('precio') is-invalid @enderror" name="precio" value="0"  autocomplete="precio">
            
                                            @error('precio')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="precioMayorista"  class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Precio mayorista') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="precioMayorista" type="number" step="0.01" class="form-control @error('precioMayorista') is-invalid @enderror" name="precioMayorista" value="0"  autocomplete="precioMayorista">
            
                                            @error('precioMayorista')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="precioVidriero"  class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Precio vidriero') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="precioVidriero" type="number" step="0.01" class="form-control @error('precioVidriero') is-invalid @enderror" name="precioVidriero" value="0"  autocomplete="precioVidriero">
            
                                            @error('precioVidriero')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="moneda" class=" col-md-4 col-form-label text-md-right"><b>{{ __('Moneda ($)') }}</b></label>
                    
                                        <div class="col-md-6">
                                            
                                                <select class="custom-select" id="inputGroupSelect01" name="moneda">
                                                  
                                                        @foreach ($monedas as $moneda)
                                                            <option value="{{$moneda->nombre}}">{{$moneda->nombre}}</option>
                                                        @endforeach
                                                </select>
                                                                  
                                            @error('moneda')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

            
                                    <div class="form-group row">
                                        <label for="stock" class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Stock') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="stock" type="number" min="1" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock') }}"  autocomplete="stock">
            
                                            @error('stock')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="categoria_id" class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Categoria') }}</b></label>
            
                                        <div class="col-md-6">
            
                                            <select class="form-control" name="categoria_id" id="categoria_id">
            
                                                @foreach ($categorias as $categoria)
                                                    <option value="{{ $categoria->id }}">{{$categoria->nombre}}</option>                                        
                                                @endforeach
            
                                            </select>

                                            <small><a href="{{route('crearCategoria')}}" class="text-primary">Crear nueva categoría</a></small>
                        
                                            @error('categoria_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>     
                                    
                                    
                            
            
                                    <div class="form-group row">
                                        <label for="productoimagen" class="tituloArticulo col-md-4 col-form-label text-md-right"><b>{{ __('Cargar imágen') }}</b></label>
            
                                        <div class="col-md-6">
                                            <input id="productoimagen" type="file" class="form-control @error('productoimagen') is-invalid @enderror" name="productoimagen" value="{{ old('productoimagen') }}" required autocomplete="productoimagen" autofocus>
            
                                            @error('productoimagen')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
            
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-success">
                                                {{ __('Agregar') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
       
</div>
    
@endsection