@extends('layouts.app')

@section('content')
            
<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">

            <div class="card-header text-center border"><h3>{{ __('Productos') }}</h3>    </div>
            <br>

            
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif
            
            <div class="row">
                
                <div class="col">
                            @foreach ($categorias as $categoria)
                                <a class=" pr-1 pl-1  text-primary rounded-pill border" style="background-color: #ffffff;" href="{{route('productosCategoria',$categoria)}}">
                                {{$categoria->nombre}}
                                </a> 
                            @endforeach
                </div>

                
                    <form method="GET" action="{{route('buscarProducto')}}" class="form-inline" id="busquedaProducto">
                        @csrf
                        <input name="busqueda" class="form-control mr-sm-2" type="search" placeholder="Buscar producto.." aria-label="Search">
                        <button class="btn btn-success"  form="busquedaProducto">
                        <i class="fas fa-search"></i>
                        </button>
                    </form>
            </div>

    
           
            <ul class="list-group text-center">
    
        
                @forelse ($productos as $producto)
                <br>
                     <li class="list-group-item">
                         <div class="row">

                             <div class="col-6">
                                <b>COD: </b>{{$producto->codigoProducto}} - {{ $producto->nombre }} 
                             </div>  

                             <div class="col-6">
                                <div class="row d-flex justify-content-end">
                                    
                                    <div class="col-3 text-center">
                                       <a href=" {{route('modificarProducto', $producto)}}">
                                           <button class="btn btn-warning" type="submit"> 
                                               <i class="fas fa-pencil-alt"></i>	
                                           </button> 
                                       </a>
                                   </div>
                                   
                                   <div class="col-3 text-center">
                                       <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('eliminarProducto', $producto)}}" method="POST" name="formulario" >
                                           @csrf
                                           @method('DELETE')
                                           <button class="btn btn-danger" type="submit"  >  
                                               <i class="fas fa-trash"></i>
                                           </button>                                      
                                       </form> 
                                   </div>
                                </div>                                                   
                           </div>

                         </div>

                         <div class="row text-center pt-2">

                            <div class="col-4 col-sm-4 col-md-2">
                                <img src="{{asset("/storage/$producto->foto")}}"  style="max-width: 100px" class="img-fluid">
                            </div>                                


                            <div class="col-4 col-sm-4 col-md-3">
                                
                               @if($producto->moneda == "Dolar")
                                    <b>CF: </b>u$s {{$producto->precio}}
                                    <br>
                                    <b>V: </b> u$s {{$producto->precioVidriero}}
                                    <br>
                                    <b>M: </b> u$s {{$producto->precioMayorista}}
                               @else
                                    <b>CF: </b>$ {{$producto->precio}}
                                    <br>
                                    <b>V:  </b> $ {{$producto->precioVidriero}}
                                    <br>
                                    <b>M: </b> $ {{$producto->precioMayorista}}
                               @endif                                                       
                            </div>

                            <div class="col-4 col-sm-2 col-md-1">
                                <b>Stock:</b>  {{$producto->stock}}
                            </div>

                          
                        
                            <div class="col-12 col-sm-12 col-md-6 py-3 text-center">
    
                                <form method="POST" action="{{ route('agregarAPresupuesto', $producto)  }}">
                                    @csrf
                                    <div class="row">

                                        <div class="col-12 col-sm-6">
                                           
                                            <select class="form-control" name="tipoCliente" id="">
                                                <option value="cf">Consumidor final (CF)</option>
                                                <option value="m">Mayorista (M)</option>
                                                <option value="v">Vidriero (V)</option>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6">                                                                 

                                            @switch($producto->categoria->nombre)                                                
                                                @case('Espejos')
                                                    <label for=""><b>M2   </b> </label> 
                                                @break

                                                @case('Vidrios')
                                                    <label for=""><b>M2   </b> </label> 
                                                @break

                                                @case('Tratamientos espejos')
                                                    <label for=""><b>ML   </b> </label> 
                                                @break

                                                @default
                                                    <label for=""><b>Cant</b> </label> 
                                            @endswitch

                                            <input 
                                                @if($producto->categoria->nombre == 'Espejos' || $producto->categoria->nombre == 'Vidrios' || $producto->categoria->nombre == 'Tratamientos espejos')
                                                type="number" {{-- accá iría -> type="" --}}
                                                @else
                                                type="number"
                                                @endif
                                            step="any" min="1" name="cantidad" required class="col-sm-4">
                                            
                                            
                                            <button type="submit"  class="btn btn-success ">+</button>
                                        </div>
                                        
                                    </div>
                                    

                                </form>

                             </div>

                             


                         </div>   
                         
                         
                     </li>
                @empty

                <div class="row d-flex justify-content-center mt-5 text-center">
    
                    <div class="col-sm-12">                            
                        <h3>No existen registros.</h3>
                    </div>
                    <div class="col-sm-12">                            
                        <a class="text-primary" href="{{route('formularioProducto')}}">Click aquí para registrar productos!</a>
                    </div>
                    
                    <br>
                    
                </div>


                
                @endforelse
                
            </ul>

            <br>
            
            {{$productos->links()}}   
                                 
        </div>
    </div>
       
</div>

@endsection