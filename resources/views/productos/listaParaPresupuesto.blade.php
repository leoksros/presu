@extends('layouts.app')

@section('content')



<div class="container"> 

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">
            
            <div class="card-header text-center border mb-3"><h3>{{ __('Productos') }}</h3> 
            <br>
                <h5> 
                    <a href="{{route('verPresupuesto',$presupuesto->id)}}">Volver a presupuesto 
                    <button class="btn btn-primary">
                    <i class="fas fa-eye"></i>
                    </a>  
                </h5>
            </div>

            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif
            

            <div class="row">
             
                <div class="col-sm-8">
                          
                
                </div>
                <div class="col-sm-4">
                    <form method="GET" action="{{route('buscarProductoParaPresupuestos',$presupuesto)}}" class="form-inline my-2 my-lg-0 " id="busquedaProducto">
                        @csrf
                        <input name="busqueda" class="form-control mr-sm-2" type="search" placeholder="Buscar producto.." aria-label="Search">
                        <button class="btn btn-success my-2 my-sm-0"  form="busquedaProducto">
                        <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>

        
    
           
            <ul class="list-group">
                
               
                
                @forelse ($productos as $producto)
                <br>         
                     <li class="list-group-item">
                         <div class="row">
    
                            <div class="col-6">
                                <b>COD: </b>{{$producto->codigoProducto}} - {{ $producto->nombre }} 
                             </div>                 
                             
                             <div class="col-6">
                                <div class="row d-flex justify-content-end">
                                    
                                    <div class="col-3 text-center">
                                       <a href=" {{route('modificarProducto', $producto)}}">
                                           <button class="btn btn-warning" type="submit"> 
                                               <i class="fas fa-pencil-alt"></i>	
                                           </button> 
                                       </a>
                                   </div>
                                   
                                   <div class="col-3 text-center">
                                       <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('eliminarProducto', $producto)}}" method="POST">
                                           @csrf
                                           @method('DELETE')
                                           <button class="btn btn-danger" type="submit">  
                                               <i class="fas fa-trash"></i>
                                           </button>                                      
                                       </form> 
                                   </div>
                                </div>                                                             
                           </div>

                        </div>

                        <div class="row text-center pt-2">

                            <div class="col-4 col-sm-4 col-md-2">
                                <img src="{{asset("/storage/$producto->foto")}}"  style="max-width: 100px" class="img-fluid">
                            </div>

                             <div class="col-4 col-sm-4 col-md-3">
                                
                                @if($producto->moneda == "Dolar")
                                    <b>CF: </b>u$s {{$producto->precio}}
                                    <br>
                                    <b>V: </b> u$s {{$producto->precioVidriero}}
                                    <br>
                                    <b>M: </b> u$s {{$producto->precioMayorista}}                                
                                @else
                                    <b>CF: </b>$ {{$producto->precio}}
                                    <br>
                                    <b>V:  </b> $ {{$producto->precioVidriero}}
                                    <br>
                                    <b>M: </b> $ {{$producto->precioMayorista}}
                                @endif
                        
                             </div>


                            <div class="col-4 col-sm-2 col-md-1">
                              <b>Stock:</b>   {{$producto->stock}}
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 py-3 text-center">
    
                                <form method="POST" action="{{ route('addItemPresupuestoGuardado', compact('producto','presupuesto') )  }}">
                                    @csrf
                                    <div class="row">

                                        <div class="col-12 col-sm-6">

                                            <select class="form-control" name="tipoCliente" id="">
                                                <option value="cf">Consumidor final (CF)</option>
                                                <option value="m">Mayorista (M)</option>
                                                <option value="v">Vidriero (V)</option>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6">
                                           
                                            @switch($producto->categoria->nombre)
                                                
                                                @case('Espejos')
                                                <label for=""><b>M2   </b> </label> 
                                                @break

                                                @case('Vidrios')
                                                <label for=""><b>M2   </b> </label> 
                                                @break

                                                @case('Tratamientos espejos')
                                                <label for=""><b>ML   </b> </label> 
                                                @break

                                                @default
                                                <label for=""><b>Cant</b> </label> 
                                            @endswitch

                                            <input 
                                            
                                            @if($producto->categoria->nombre == 'Espejos' || $producto->categoria->nombre == 'Vidrios' || $producto->categoria->nombre == 'Tratamientos espejos')
                                                type="number" {{-- accá iría -> type="" --}}
                                            @else
                                                type="number"
                                            @endif
                                            
                                            step="any" min="1" name="cantidad" required class="col-sm-4">
                                            <button type="submit"  class="btn btn-success "> + </button>

                                        </div>
                                        
                                    </div>
                                    

                                </form>

                             </div>



    
                            

                            
                            
                         </div>   
                         
                         
                     </li>
                @empty
                <div class="row d-flex justify-content-center text-center">
    
                    <div class="col-sm-12">                            
                        <h1 class="tituloArticulo">No hay productos cargados.</h1>
                     </div>
                    <div class="col-sm-12">
                        <a href="{{route('formularioProducto')}}">Click aquí para registrar productos!</a>
                    </div> 
                </div>
                @endforelse
                
            </ul>
            <br>
            {{$productos->links()}}
        </div>
    </div>
       
</div>


@endsection