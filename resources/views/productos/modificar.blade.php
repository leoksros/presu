@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center"><h3>{{ __('Actualizar artículo') }}</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registrarUpdate', $producto) }}" enctype="multipart/form-data">                       
                        @csrf
                        @method('PUT')

                        <div class="row">

                            <div class="col">
                                <div class="form-group row">
                                    <label for="nombre" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>
        
                                    <div class="col-md-6">
                                        <input  id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{$producto->nombre}}" required autocomplete="nombre" autofocus>
        
                                        @error('nombre')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="codigoProducto" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Código') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="codigoProducto" type="text" class="form-control @error('codigoProducto') is-invalid @enderror" name="codigoProducto" value="{{ $producto->codigoProducto }}" required autocomplete="codigoProducto" autofocus>
        
                                        @error('codigoProducto')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="descripcion" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{$producto->descripcion}}" required autocomplete="descripcion" autofocus>
        
                                        @error('descripcion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div> 
        
                                <div class="form-group row">
                                    <label for="precio" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Precio c. final') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="precio" type="text" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{$producto->precio}}" required autocomplete="precio" autofocus>
        
                                        @error('precio')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>


                                
                                <div class="form-group row">
                                    <label for="precioMayorista"  class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Precio mayorista') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="precioMayorista" type="number" step="0.01" class="form-control @error('precioMayorista') is-invalid @enderror" name="precioMayorista" value="{{$producto->precioMayorista}}" required autocomplete="precioMayorista">
        
                                        @error('precioMayorista')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="precioVidriero"  class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Precio vidriero') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="precioVidriero" type="number" step="0.01" class="form-control @error('precioVidriero') is-invalid @enderror" name="precioVidriero" value="{{$producto->precioVidriero}}" required autocomplete="precioVidriero">
        
                                        @error('precioVidriero')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="moneda" class=" col-md-4 col-form-label text-md-right">{{ __('Moneda ($)') }}</label>
                
                                    <div class="col-md-6">                                            
                                        
                                            <select class="custom-select" id="inputGroupSelect01" name="moneda" required>
                                              <option selected>Seleccionar</option>
                                                  

                                                    @foreach ($monedas as $moneda)
                                                        @if ($moneda->nombre == $producto->moneda)
                                                         <option selected value="{{$moneda->nombre}}">{{$moneda->nombre}}</option> 
                                                         @else
                                                         <option value="{{$moneda->nombre}}">{{$moneda->nombre}}</option>     
                                                        @endif                                                                                                                                                      
                                                    @endforeach



                                            </select>
                                                              
                                        @error('moneda')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="stock" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Stock') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="stock" type="number" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{$producto->stock}}" required autocomplete="stock">
        
                                        @error('stock')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="categoria_id" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Categoria') }}</label>
        
                                    <div class="col-md-6">
                                        <select class="form-control" name="categoria_id" id="categoria_id">

                                            @foreach ($categorias as $categoria)
                                                        @if ($categoria->nombre == $producto->categoria->nombre)
                                                         <option selected value="{{$categoria->id}}">{{$categoria->nombre}}</option> 
                                                         @else
                                                         <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>     
                                                        @endif                                                                                                                                                      
                                            @endforeach


                                            {{-- @foreach ($categorias as $categoria)
                                                <option value="{{ $categoria->id }}">{{$categoria->nombre}}</option>                                        
                                            @endforeach --}}
        
                                        </select>
                                        @error('categoria_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                                  
        
                                
                                
        
                                
                            </div>

                            <div class="col-sm-12 col-lg-5">
                                
                                    <img src="{{asset("/storage/$producto->foto")}}"  alt="" class="img-fluid align-middle">
                                
                                <div class="form-group row">
                                    <label for="productoimagen" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Cambiar imágen') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="productoimagen" type="file" class="form-control @error('productoimagen') is-invalid @enderror" name="productoimagen" value="{{ old('productoimagen') }}" autocomplete="productoimagen" autofocus>
        
                                        @error('productoimagen')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>                          
                            </div>

                            

                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Guardar cambios') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection