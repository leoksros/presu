@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">

            <div class="card-header text-center border"><h3>{{ $producto->nombre }}</h3></div>
            <br>
               
        <div class="row">
            <div class="col-sm-12 col-md-7 col-lg-6">
                <img  src="{{asset("/storage/$producto->foto")}}"  alt="" class="img-fluid"> 
            </div>
    
            <div class="col-sm-12 col-md-7 col-lg-6">
    
                <div class="row">
                        <div class="col mt-3 mb-3 font-weight-bold">
                        <p class="h3">Categoria: {{$producto->categoria->nombre}}</p>                        
                        </div>
                </div>
    
                
                <div class="row">
                    <div class="col">
    
                        <br>
                        <p><b>Código:</b> {{$producto->codigoProducto}}</p>        
                        <p><b>Descripcion:</b> {{$producto->descripcion}}</p>                               
                        <p><b>Valor: </b>{{$producto->precio}}</p>
                        <p><b>Moneda:</b> {{$producto->moneda}}</p>
                        <p><b>Stock:</b> {{$producto->stock}} </p>
                                                                                                                   
                        <br>
                    </div>                        
                </div> 
                    
                <div class="d-flex justify-content-center mt-3">                    
                    <a href="{{route('modificarProducto', $producto)}}"><button class="btn btn-success btn-lg">Modificar</button></a> 
                </div>
                    
               
            </div>  
        </div>
         
           
            
        </div>
    </div>
       
</div>


@endsection