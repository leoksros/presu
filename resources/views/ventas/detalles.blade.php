@extends('layouts.app')


@section('content')

<div class="container">

    


    <div class="card-header text-center border pb-2"><h1>Venta #{{$venta->id}}</h3> <a class="text-primary h5" href="{{route('verVenta',$venta->id)}}">Volver</a></div>

    @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                       
                    </div>
    @endif

    <div class="row pt-5">
        <div class="col-9">
            <div class="card-body  border">

                <h5 class="h3 font-weight-bold card-title mb-4">Entregas</h5>

                        <div class="row d-flex my-2 card-header border pb-2">
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-2 font-weight-bold">Creación</div>
                                    <div class="col-5  text-center font-weight-bold">$</div>
                                    <div class="col-5  text-center font-weight-bold">Descripción</div>
                                </div>
                            </div>

                            <div class="col-2 text-center">
                                <div class="col font-weight-bold">Borrar</div>
                            </div>
                        </div>

                        @forelse ($venta->entregas as $entrega)
                                <div class="row d-flex my-2 border">

                                        <div class="col-10">
                                            <div class="row">
                                                <div class="col-2"><small>{{$entrega->created_at}}</small></div>
                                                <div class="col-5  text-center"><b>${{$entrega->entrega}}</b></div>
                                                <div class="col-5  text-center">{{$entrega->detalle}}</div>
                                            </div>
                                        </div>
                                 
                                        <div class="col-2 py-2 text-center">
                                                <form onclick="return confirm('Está seguro de eliminar la entrega?')" action="{{route('borrarEntrega', $entrega)}}" method="POST" >
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger" type="submit">  
                                                                X
                                                        </button>   
                                                </form>
                                        </div>
                                                                    
                                </div>

                        @empty
                            <div class="text-center py-5">
                                <h1>No se registran entregas.</h1>

                            </div>
                        @endforelse 
                                                                   
            </div>
        </div>
        <div class="col-3">
            <div class="card-body  border">
                <h4 class="card-title h3 font-weight-bold">Saldo</h5>

                        <div class="row d-flex">
                            
                            <div class="col-12 py-3">
                                <span class="h4 text-danger font-weight-bold">Saldo deudor: {{ $venta->saldoRestanteNuevo() }}</span>

                            </div>
                                <div class="col">

                                        <form  method="POST" action="{{route('registrarEntrega',$venta)}}">

                                                        @csrf
                                                        <span class=" font-weight-bold">Entrega parcial: </span>
                                                        <input name="entregaParcial" class="form-control" type="number" required step="any" min="0">
                                                        <br>
                                                        <span class=" font-weight-bold">Detalle entrega: </span>
                                                        <input name="detalleEntrega" class="form-control" type="text">
                                                        <br>
                                                        <button onclick="return confirm('Confirmar entrega.')" type="submit" class="btn btn-success form-control">Registrar entrega</button>
                                       
                                        </form>
                                                  
                                </div>
                

                        </div>
                        <br>
                       


                </div>
        </div>
    </div>



    

    


</div>



@endsection