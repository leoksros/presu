@extends('layouts.app')


@section('content')
    

@php
    
@endphp
                
<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">
            
            <div class="card-header text-center border"><h3>{{ __('Ventas') }}</h3>    </div>

            <br>
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif


            <div  class="rowVenta border shadow mt-3 px-3 py-3 row">

                <div class="col text-center">
                   <b>Número</b>
                </div>

                <div class="col text-center">
                  <b>Cliente</b>                                
                </div>
            
                 <div class="col text-center">
                    <b>Fecha</b>
                 </div>

                 <div class="col text-center"> 
                    <b>Acciones</b> 
                 </div>
                 
            </div>

    
            <ul class="list-group">
    
               
                
                @forelse ($ventas as $venta)
                <br>
            
                     <li class="list-group-item">
                         <div class="d-flex row text-center">
                             
    
                             <div class="col">
                                  
                              <b>{{$venta->id}} </b> 
                             </div>                        

                            <div class="col">
                                @if($venta->cliente_id != null)                                
                                   <a class="stretched-link text-primary" href="{{route('verCliente',$venta->cliente)}}">{{ $venta->cliente->nombre }}</a> 
                                @else
                                    {{ $venta->nombreCliente }}
                                @endif
                            </div>


                             <div class="col">
                                {{$venta->created_at}}
                             </div>

                             <div class="col">
                                 <div class="row">
                                     
                                     <div class="col"> 
                                            <button class="btn btn-primary">
                                                <a href="{{route('verVenta',$venta->id)}}">
                                                <i class="fas fa-eye"></i>
                                            </a>  
                                     </div>

                                     <div class="col"> 
                                        <a target="_blank" href="{{route('pdfVenta',$venta->id)}}"> 
                                           <button class="btn btn-success"> 
                                               <i class="fas fa-arrow-down"></i>	
                                           </button> 
                                       </a> 
                                   </div>
                                    
                                     <div class="col"> 
                                         <a href="{{route('verVenta',$venta->id)}}">
                                            <button class="btn btn-warning"> 
                                                <i class="fas fa-pencil-alt"></i>	
                                            </button> 
                                         </a> 
                                     </div>

                                     

                                     <div class="col">
                                        <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('borrarVenta',$venta->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"> 
                                                <i class="fas fa-trash"></i>
                                            </button>                             
                                        </form>
                         
                                    </div>
                                 </div>
                             </div>

                            
                         </div>   
                         
                         
                     </li>

                @empty

                <div class="row d-flex justify-content-center mt-5 text-center">                                             
                        <h3>No existen registros.</h3>
                </div>
                
                @endforelse
                
            </ul>
            <br>
          {{$ventas->links()}}
        </div>
    </div>
       
</div>



@endsection


