@extends('layouts.app')


@section('content')
                
<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">

            <div class="card-header text-center border"><h3>{{ __('Saldos pendientes') }}</h3></div>
   
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div class="row pt-3 justify-content-around">
              <div class="col-5 btn btn-success text-center">
                <a href="{{route('obtenerVentasNotificadas','desc')}}">Notificados</a> 
              </div>
              <div class="col-5 btn btn-danger text-center">
                <a href="{{route('obtenerVentasNoNotificadas','desc')}}">No notificados</a> 
              </div>
            </div>

            <div  class="rowVenta border shadow mt-3 px-3 py-3 row">

                            <div class="col text-center">
                               <b>Número</b>
                            </div>

                            <div class="col text-center">
                              <b>Cliente</b>                                
                            </div>
                        
                             <div class="col text-success text-center">
                                <b>Entregado</b>                           
                             </div>

                             <div class="col text-danger text-center">
                                <b>Pendiente</b> 
                             </div>
    
     
                             <div class="col text-center">
                                @if(basename(url()->current()) != 'asc')    
                                    <a href="{{route('saldosPendientes','asc')}}"><b>Fecha</b></a>
                                  @else
                                    <a href="{{route('saldosPendientes','desc')}}"><b>Fecha</b></a>
                                @endif  
                             </div>

                             <div class="col text-center"> 
                                <b>Acciones</b> 
                             </div>
                             
            </div>
            
            <ul class="list-group">
              
                @forelse ($cuentas as $cuenta)
                <br>
                     <li class="list-group-item">
                         <div  class="rowVenta d-flex row justify-content-center">
                             
                            
                            <div class="col text-center">
                              <a href="{{route('verVenta',$cuenta)}}"><b>{{$cuenta->id }}</b></a> 
                            </div>

                            <div class="col text-center">
                              <b></b>  
                                
                                @if($cuenta->cliente_id != null)                                
                                  <a class="stretched-link text-primary" href="{{route('verCliente',$cuenta->cliente)}}">  {{$cuenta->cliente->nombre}}</a>
                                @else
                                    {{$cuenta->nombreCliente}}
                                @endif
                            </div>
                        
                             <div class="col text-success text-center">
                                {{$cuenta->sumaEntregas()}}                         
                             </div>

                             <div class="col text-danger text-center">
                                {{$cuenta->total() - $cuenta->sumaEntregas()}}
                             </div>
                             

                             <div class="col text-center">
                              {{$cuenta->created_at}}
                             </div>

                             <div class="col"> 
                               <div class="row">
                                 <div class="col">
                                    <button class="btn btn-primary">
                                      <a href="{{route('verVenta',$cuenta->id)}}">
                                      <i class="fas fa-eye"></i>
                                      </a>  
                                    </button>
                                 </div>
                                 <div class="col">
                                  @if (count($cuenta->notificaciones) == 0)
                                  <button type="button" id="{{$cuenta}}" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap"><i class="fas fa-plus"></i></button>
                                      @else
                                        <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('borrarNotificacion',$cuenta)}}" method="POST">
                                          @csrf
                                          @method('DELETE')
                                          <button class="btn btn-success"> 
                                              <i class="fas fa-minus"></i>
                                          </button>                             
                                        </form>
                                  @endif
                                 </div>
                               </div>
                             </div>
                             
                         </div>   
                         
                         
                     </li>

                @empty

                <div class="row d-flex justify-content-center mt-5">
    
                    <h3> No existen registros guardados para esta sección.</h3>
                </div>
                @endforelse
                
            </ul>

            <br>
            {{$cuentas->links()}}
        </div>
    </div>

    
</div>

{{-- modal --}}

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Notificación</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{route('guardarNotificacion')}}" id="formularioNotificacion">              
            @csrf
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Descripición:</label>
              <input type="text" class="form-control" name="descripcion" id="recipient-name">
            </div>

            {{-- Comentado para incorporar en caso de necesitar agregar recordatorio de aviso segun fecha --}}

            {{-- <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Fecha:</label>
                    <input type="date" required class="form-control" name="fecha" id="recipient-name">
            </div> --}}

          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" form="formularioNotificacion" class="btn btn-primary">Guardar</button>
        </div>

      </div>
    </div>
  </div>
  
{{-- modal --}}

@endsection
