@extends('layouts.app')


@section('content')


<div class="container">
    
    <div class="card-header text-center border"><h3>{{ __('Venta') }}</h3>    </div>

    <br>


    @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                       
                    </div>
    @endif

    <div class="row">
            
            <div class="col col-md-7">

                <div class="card w-100 mb-4">
                        <div class="card-body">
                                
                                <form method="POST" action="{{route('actualizarVenta',$venta)}}" id="formulario">

                                        @csrf
                                        

                                        <div class="card-header border">

                                                <h4>Datos: </h4>
                                        </div>
                                        <br>
                                        @if($venta->cliente == null)

                                                <br>
                                                <label for="">Cliente: </label><input class="form-control" value="{{$venta->nombreCliente}}" name="nombreCliente" type="text">
                                                <br>
                                                <label for="">Email:</label><input class="form-control" value="{{$venta->email}}" name="email" type="text">
                                                <br>
                                                <label for="">Aclaracion:</label><input class="form-control" value="{{$venta->descripcion  }}" name="descripcion" type="text">
                                        @else 
                                        <div class="row mt-3">
                                                        
                                                <div class="col-sm-6">
                                                        <label for="">Cliente:</label>        
                                                        <input   readonly   class="form-control" name="nombreCliente" type="text" value="{{$venta->cliente->nombre}}">
                                                          
                                                </div>
                                                <div class="col-sm-6">
                                                        <label for="">CUIT:</label><input  readonly    class="form-control"  name="cuit" type="text" value="{{$venta->cliente->cuit}}">
                                                </div>
                                                
                                                <div class="col-sm-6">
                                                        <label for="">Direccion:</label><input  readonly    class="form-control"  name="direccion" type="text" value="{{$venta->cliente->direccion}}">

                                                </div>
                                                <div class="col-sm-6">
                                                        <label for="">Banco:</label>        
                                                        <input   readonly   class="form-control" name="nombreCliente" type="text" value="{{$venta->cliente->banco}}">
                                                          
                                                </div>
                                                
                                                
                                                <div class="col-sm-6">
                                                        <label  for="">Email:</label><input   readonly  class="form-control"  name="email" type="text" value="{{$venta->cliente->email}}">
                                                </div>
                                                
                                                
                                                <div class="col-sm-6">
                                                        <label  for="">CBU:</label><input  readonly  class="form-control"  name="cbu" type="text" value="{{$venta->cliente->cbu}}">

                                                </div>
                                                <div class="col-sm-6">
                                                        <label  for="">Aclaracion:</label><input  readonly class="form-control"  name="descripcion" type="text" value="{{$venta->cliente->descripcion}}">

                                                </div>
                                                <div class="col-sm-6">
                                                        <label  for="">Alias:</label><input readonly class="form-control"  name="alias" type="text" value="{{$venta->cliente->alias}}">

                                                </div>
                                               
                                        </div>
                                        @endif
                                        <br><br>
                                        <div class="">

                                                <div class="mb-3 card-header border"> 

                                                                <h4>Ajustes ($):</h4>
                                                       
                                                </div>

                                                <div class="row d-flex">

                                                        <div class="col-sm-6">
                                                                <label for="">Recargo (%):</label>
                                                                <input class="form-control"  name="recargo" type="number" step="0.01" min="0" value="{{$venta->recargo}}">
                                                        </div>
                                                        <div class="col-sm-6">
                                                                <label for="">IVA (%):</label>
                                                                <input class="form-control"  name="iva" type="number" step="0.01" min="0" value="{{$venta->iva}}">
                                                        </div>
                                                        <div class="col-sm-6">
                                                                <label for="">Adicional ($):</label>
                                                                <input class="form-control"  name="adicional" type="number" step="0.01" min="0" value="{{$venta->adicional}}">
                                                        </div>

                                                        <div class="col-sm-6">
                                                                <label for="">Tipo cliente:</label>
                                                                <select class="custom-select" id="inputGroupSelect01" name="tipoCliente">                                                                                        
                                                                        

                                                                        @foreach ($tiposcliente as $tipo)
                                                                                @if ($tipo->nombre == $venta->tipocliente->nombre)
                                                                                 <option selected value="{{$tipo->id}}">{{$tipo->nombre}}</option> 
                                                                                 @else
                                                                                 <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>     
                                                                                @endif                                                                                                                                                      
                                                                        @endforeach          
                                                                             
                                                                      </select>
                                                        </div>

                                                        <div class="col-sm-6">
                                                                <label for="">Valor dolar asignado:</label>
                                                                <input class="form-control"  name="valorDolar" type="number" step="any" min="0" value="{{$venta->valorDolar}}">
                                                        </div>

                                                </div>
                                                <br>
                                        
                                        </div>
                                </form>

                                        <div class="card-header border">

                                                <h4>
                                                        Productos: 
                                                        <a  class="btn btn-primary" href="{{route('listaProductosVenta',$venta)}}">
                                                                Agregar <i class="fas fa-plus"></i>
                                                        </a>
                                                </h4>    
                                        </div>
                                        <br>
                                        
                                        @forelse ($venta->items as $item)

                                                {{--  --}}
                                                <div class="card w-100 mb-4">
                                                
                                                        <div class="card-body">
                                                                <h5 class="card-title ">
                                                                        <a class="tituloArticulo" href="{{-- {{route('producto', $item->producto)}} --}}">{{$item->producto->nombre}} // Cod: {{$item->producto->codigoProducto}}</a>
                                                                        
                                                                </h5>
                                                                
                                                                <div class="row ">
                
                                                                        <div class="col col-sm-4">                                                                                 
                                                                                <img src="{{asset("/storage/".$item->producto->foto)}}" class="img-fluid tamanioArticulo">                                                                                                                                                                                    
                                                                        </div>
                
                                                                        <div class="col col-sm-8">        
                                                                                <form method="GET" action="{{ route('updateItemVenta',$item)}}" class="form-inline my-2 my-lg-0 " >
                                                                                        @csrf
                                                                                        
                                                                                
                                                                                        <div class="col-sm-12 mb-2">
                                                                                                <p class="card-text"> {{$item->producto->descripcion}}</p>
                                                                                        </div>
                                                                                        
                                                                                        <div class="col-sm-12">
                                                                                        
                                                                                                <p> <b>Cantidad:            </b> 
                                                                                                
                                                                                                <input step="any" type="number" class="form-control" name="cantidad" value="{{$item->cantidad}}"></p>
                                                                                        </div>
                                                                                        
                                                                                
                
                                                                                        @if($item->producto->moneda == 'Dolar')
                                                                                        <div class="col-sm-12">
                                                                                                <p> <b>Precio unit (u$s):</b> 
                                                                                                        <input step="any" type="number" readonly class="form-control" name="precio" value="{{$item->producto->precio}}"></p>
                                                                                        </div>
                                                                                        @endif
                                                                                        <div class="col-sm-12">
                                                                                                <p> <b>Precio unit ($):    </b> 
                                                                                                        <input step="any" type="number" readonly class="form-control" name="precio" value="{{$item->precio}}"></p>
                                                                                        </div>
                
                                                                                        <div class="col-sm-12">
                                                                                                <p>                                                         
                                                                                                        <b>Suma:</b>
                                                                                                
                                                                                                        @php 
                                                                                                                echo( $item->precio*$item->cantidad); 
                                                                                                        @endphp 
                                                                                                </p>
                                                                                        </div>
                                                                                                
                                                                                
                                                                                        <div class="container">
                
                                                                                                <div class="row justify-content-end">
                                                                                                
                                                                                                        <div class="col-4">
                                                                                                                <button class="btn btn-warning" type="submit" > 
                                                                                                                        <i class="fas fa-sync-alt"></i>
                                                                                                                </button>  
                                                                                                        </div>                                                                                                                                                                                                                
                                                                                                        
                                                                                                        <div class="col-4">
                                                                                                                <a onclick="return confirm('Está seguro de quitar el producto de la venta?')" class="btn btn-danger" href="{{route('removerProductoVenta',$item)}}"> <i class="fas fa-trash"></i> </a>                                                                                                                                                          
                                                                                                        </div>
                                                                                                                                                                                                                
                                                                                                                                                                                                
                                                                                                </div>                    
                                                                                        </div>
                                                                                                                                                                                
                                                                                                
                                                                                </form>   
                                                                        </div>             
                                                                </div>
                                                        
                                        
                                                        </div>
                                                                
                                                </div>
                                                {{--  --}}
                                       
                                      
                            {{-- <div class="card w-100 mb-4">
                   
                                    <div class="card-body">
                                            <h5 class="card-title ">
                                                    <a class="tituloArticulo" href="">{{$item->producto->nombre}}</a>
                                            </h5>
                                            
                                            <div class="row">

                                                    <div class="col col-sm-4">                                                                                 
                                                        <img src="{{asset("/storage/".$item->producto->foto)}}" class="img-fluid tamanioArticulo">                                                                                                                                                                                    
                                                    </div>

                                                    <div class="col col-sm-8">
                                                    <p class="card-text"> {{$item->producto->descripcion}}</p>



                                                        

                                                            <p> <b>Cantidad:</b> <input type="number" class="form-control" name="cantidad.{{$item->id}}" value="{{$item->cantidad}}"></p>
                                                            
                                                            <p> <b>Precio :</b> <input type="number" class="form-control " readonly name="precio.{{$item->id}}" value="{{$item->precio}}"></p>
                                                        
                                                       
                                                            <p> <b>Suma:</b> @php echo( $item->precio*$item->cantidad); @endphp </p>

                                                            <div class="row d-flex justify-content-around">
                                                                
                                                                <div class="col-md-3 ml-md-auto">                                                                                        
                                                                        <a class="btn btn-danger" href="{{route('removerProductoVenta',$item)}}"> 
                                                                                <i class="fas fa-trash"></i>
                                                                             </a>                                                                                                                                                          
                                                                </div>                                                                    
                                                            </div>                     
                                                                               
                                                    </div>
                                            </div>
                                    
                    
                                    </div>
                            </div> --}}
                    @empty

                    
                    <div class="row d-flex justify-content-center">

                            <h1 class="tituloArticulo">Venta vacía.</h1>
                          </div>

                    @endforelse





                                
                        </div>

                </div>                

                    

            </div>
    
            <div class="col-sm-12 col-md-4 color-marron-letra ">
                    <div class="card" style="width: 18rem;">
                            <div class="card-body  border">
                            <h4 class="h3  font-weight-bold mb-4">Venta</h5>

                                    <div class="row d-flex">
                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                            
                                                            <span class=" font-weight-bold">Subtotal: </span>
                                                            <br><br>                                                              
                                                            <span class=" font-weight-bold">Total:</span>

                                            </div>

                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                                                    
                                                            <span class="">${{$venta->subtotal()}}</span>
                                                            <br><br>                                                  
                                                            <span class="">$ {{$venta->total()}}</span>
                                            </div>

                                        
                                            <div class="col-sm-12 mt-5">        
                                                <button onclick="return confirm('Confirmar cambios.')" type="submit" class="btn btn-primary  form-control" form="formulario"> 
                                                        Registrar cambios
                                                </button> 
                                            </div>
                                            @if($venta->iva == '21')
                                            <div class="col-sm-12 mt-3">                                                                                                        
                                                        <form method="POST" action="{{route('facturarVenta',$venta)}}">
                                                                @csrf
                                                                @if($venta->facturado == false)
                                                                <button onclick="return confirm('Facturar venta?')" type="submit" class="btn btn-danger  form-control"> 
                                                                         Facturar
                                                                </button>
                                                                @else
                                                                <button onclick="return confirm('Desfacturar venta?')" type="submit" class="btn btn-secondary  form-control"> 
                                                                        Desfacturar
                                                               </button>
                                                                @endif
                                                        </form>                                                 
                                            </div>
                                            @endif

                                            
                                            
                                            
                                            
                                                                   
                                    </div>
                                    <br>
                                   
                                    <div class="border text-center py-2">
                                        <span class="h3 text-danger font-weight-bold">Saldo deudor:</span>
                                        <br>
                                        <p class="h1  text-danger font-weight-bold"><b> $ {{ $venta->saldoRestanteNuevo() }}</b></p>
                                        
                                       
                                </div>
                    

                            </div>
                            
                            <br>
                            <div class="card-body  border">
                                <h4 class="h3  font-weight-bold mb-4">Registar entrega</h5>
    
                                        <div class="row d-flex">
                                                <div class="col-sm-12">

                                                        <form  method="POST" action="{{route('registrarEntrega',$venta)}}">

                                                                        @csrf
                                                                        <span class=" font-weight-bold">Entrega parcial: </span>
                                                                        <input name="entregaParcial" class="form-control" type="number" required step="any" min="0">
                                                                        <br>
                                                                        <span class=" font-weight-bold">Detalle entrega: </span>
                                                                        <input name="detalleEntrega" class="form-control" type="text">
                                                                        <br>
                                                                        <button onclick="return confirm('Confirmar entrega.')" type="submit" class="btn btn-success form-control">Registrar entrega</button>
                                                       
                                                        </form>

                                                                  
                                                                
                                                                
                                                </div>
                                
    
                                        </div>
                                        <br>
                                       

    
                                </div>
                                <br>
                                

                               
                                <div class="card-body  border">

                                        <div class="row">
                                                <div class="col-9">
                                                       <a href="{{route('detallesSaldo',$venta)}}"><h3 class="h3  font-weight-bold mb-4">Lista entregas </h3>  </a> 

                                                </div>
                                                <div class="col-3">
                                                        <a type="button" href="{{route('detallesSaldo',$venta)}}" class="btn btn-primary"><i class="fas fa-clipboard-list"></i></a>
                                                </div>

                                        </div>
            
                                                @forelse ($venta->entregas as $entrega)
                                                        <div class="row d-flex mb-2 border">
                                                                <div class="col-sm-7 col-md-7 col-lg-7">
                                                                                
                                                                      <b> {{$entrega->created_at}}</b> 
                
                                                                </div>
                                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                                        
                                                                      <b> ${{$entrega->entrega}}</b> 
                                                                </div>
                                                                <div class="col-sm-2">


                                                                        <form onclick="return confirm('Está seguro de eliminar la entrega?')" action="{{route('borrarEntrega', $entrega)}}" method="POST" >
                                                                                @csrf
                                                                                @method('DELETE')
                                                                                <button class="btn btn-danger" type="submit">  
                                                                                        X
                                                                                </button>   
                                                                        </form>
                                                                </div>

                                                                <div class="col">
                                                                        {{$entrega->detalle}}
                                                                </div>
                                                        
                                                        </div>
                                                @empty
                                                        No se registran entregas.
                                                @endforelse 
                                                
                                                
                                           
                                </div>

                    </div>
                

            </div>

    </div>


</div>



@endsection