<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('sidebar','HomeController@show')->name('sidebar')->middleware('auth');

/* Categorias */


Route::get('listaCategorias','CategoriaController@index')->name('listaCategorias')->middleware('auth');
Route::get('categoria/crear','CategoriaController@create')->name('crearCategoria')->middleware('auth');
Route::post('categoria/registro','CategoriaController@store')->name('registrarCategoria')->middleware('auth');
Route::get('modificarCategoria/{id}','CategoriaController@edit')->name('modificarCategoria')->middleware('auth');
Route::put('modificarCategoria/{id}','CategoriaController@update')->name('modificarCategoria')->middleware('auth');
Route::delete('eliminarCategoria/{item}','CategoriaController@destroy')->name('eliminarCategoria')->middleware('auth');
Route::get('categoria/productos/{categoria}','CategoriaController@show')->name('productosCategoria')->middleware('auth');

/* Producto */

Route::get('listaProductos/search','ProductoController@search')->name('buscarProducto')->middleware('auth');
Route::get('producto/{id}','ProductoController@show')->name('producto')->middleware('auth');
Route::get('listaProductos/{tipo_ordenamiento?}','ProductoController@index')->name('listaProductos')->middleware('auth');
Route::get('listaProductos/presupuesto/{presupuesto}','ProductoController@indexPresupuestoGuardado')->name('listaProductosPresupuestoGuardado')->middleware('auth');
Route::get('agregarProducto','ProductoController@create')->name('formularioProducto')->middleware('auth');
Route::post('producto/guardar','ProductoController@store')->name('registrarProducto')->middleware('auth');
Route::get('producto/modificar/{producto}','ProductoController@edit')->name('modificarProducto')->middleware('auth');
Route::delete('abm/{producto}','ProductoController@destroy')->name('eliminarProducto')->middleware('auth');
Route::get('listaProductosPresupuesto/buscar/{presupuesto}','ProductoController@searchParaPresupuestosGuardados')->name('buscarProductoParaPresupuestos')->middleware('auth');
Route::get('listaProductosVenta/buscar/{venta}','ProductoController@searchParaVentas')->name('buscarProductoParaVenta')->middleware('auth');
Route::put('producto/{producto}','ProductoController@update')->name('registrarUpdate')->middleware('auth');



/* Presupuesto */
Route::post('presupuesto/nuevoproducto','PresupuestoController@agregarProductoProvisorio')->name('agregarAPresupuestoProductoProvisorio')->middleware('auth');
Route::post('presupuesto/{producto}','PresupuestoController@add')->name('agregarAPresupuesto')->middleware('auth');
Route::get('presupuesto/','PresupuestoController@show')->name('presupuesto')->middleware('auth');
Route::get('presupuestos/lista','PresupuestoController@index')->name('listaPresupuestos')->middleware('auth');
Route::get('removerItem/{item}','PresupuestoController@remover')->name('removerItem')->middleware('auth');
Route::get('presupuesto/store/{cliente?}','PresupuestoController@store')->name('generarPresupuesto')->middleware('auth');
Route::get('pdf','PresupuestoController@verPDF')->name('verPDF')->middleware('auth');
Route::get('presupuesto/guardado/{id}/{cliente?}','PresupuestoController@presupuesto')->name('verPresupuesto')->middleware('auth');
Route::get('presupuesto/pdf/{id}','PresupuestoController@pdf')->name('pdfPresupuesto')->middleware('auth');
Route::get('presupuesto/modificar/{rowId}','PresupuestoController@updateItem')->name('modificarItem')->middleware('auth');
Route::get('removerItemPresupuesto/{item}','PresupuestoController@destroy')->name('removerItemPresupuesto')->middleware('auth');
Route::get('presupuesto/eliminar/{id}','PresupuestoController@eliminarPresupuesto')->name('eliminarPresupuesto')->middleware('auth');
Route::post('updatePresupuesto/productoProvisorio/{presupuesto}','PresupuestoController@agregarProductoProvisorioAPresupuesto')->name('actualizarPresupuestoGuardado')->middleware('auth');

Route::post('updatePresupuesto/{presupuestoId}','PresupuestoController@updatePresupuesto')->name('actualizarPresupuesto')->middleware('auth');
Route::post('updatePresupuesto/nuevoitem/{producto}/{presupuesto}','PresupuestoController@addProducto')->name('addItemPresupuestoGuardado')->middleware('auth');
Route::get('updatePresupuesto/actualizar/{item}','PresupuestoController@updateItemPresupuesto')->name('updateItemPresupuesto')->middleware('auth');

/* Home */

Route::get('ventas/sinfacturar','HomeController@facturacionPendiente')->name('facturacionPendiente')->middleware('auth');
Route::get('valores','ValorController@index')->name('listaValores')->middleware('auth');
Route::put('valores/actualziar/{moneda}','HomeController@update')->name('actualizarValor')->middleware('auth');



/* Venta */

Route::get('ventas/listado','VentaController@index')->name('listaVentas')->middleware('auth');
Route::get('ventas/pendientes/{orden?}','VentaController@pendientes')->name('saldosPendientes')->middleware('auth');
Route::get('ventas/{presupuesto}','VentaController@store')->name('registrarVenta')->middleware('auth');
Route::get('venta/{id}','VentaController@show')->name('verVenta')->middleware('auth');
Route::post('venta/entrega/{venta}','VentaController@update')->name('registrarEntrega')->middleware('auth');
Route::post('venta/{venta}','VentaController@updateVenta')->name('actualizarVenta')->middleware('auth');
Route::get('venta/agregarproducto/{venta}','VentaController@listaProductos')->name('listaProductosVenta')->middleware('auth');
Route::post('venta/nuevoproducto/{producto}/{venta}','VentaController@addProducto')->name('addProductoVenta')->middleware('auth');
Route::delete('venta/borrar/{id}','VentaController@destroy')->name('borrarVenta')->middleware('auth');
Route::get('venta/borrarproducto/{item}','VentaController@removerProducto')->name('removerProductoVenta')->middleware('auth');
Route::get('venta/pdf/{id}','VentaController@pdf')->name('pdfVenta')->middleware('auth');
Route::post('venta/facturar/{venta}','VentaController@facturar')->name('facturarVenta')->middleware('auth');
Route::get('venta/actualizaritem/{item}','VentaController@updateItemVenta')->name('updateItemVenta')->middleware('auth');
Route::get('venta/detallesSaldo/{venta}','VentaController@verDetallesSaldo')->name('detallesSaldo')->middleware('auth');
Route::get('ventas/pendientes/notificados/{orden?}','VentaController@obtenerVentasNotificadas')->name('obtenerVentasNotificadas')->middleware('auth');
Route::get('ventas/pendientes/sinnotificar/{orden?}','VentaController@obtenerVentasNoNotificadas')->name('obtenerVentasNoNotificadas')->middleware('auth');


/* Clientes */

Route::get('clientes/lista','ClienteController@index')->name('listaClientes')->middleware('auth');
Route::get('clientes/nuevo','ClienteController@create')->name('crearCliente')->middleware('auth');
Route::post('clientes/store','ClienteController@store')->name('guardarCliente')->middleware('auth');
Route::get('clientes/ver/{cliente}','ClienteController@show')->name('verCliente')->middleware('auth');
Route::get('clientes/editar/{cliente}','ClienteController@edit')->name('editarCliente')->middleware('auth');
Route::delete('clientes/borrar/{cliente}','ClienteController@destroy')->name('borrarCliente')->middleware('auth');
Route::get('clientes/buscar/{presupuesto?}','ClienteController@search')->name('buscarCliente')->middleware('auth');
Route::get('cliente/resultado/{clientes}','ClienteController@mostrarResultado')->name('resultadoBusqueda')->middleware('auth');
Route::put('cliente/presupuesto/{presupuesto}/{cliente}','ClienteController@agregarClientePresupuesto')->name('asignarCliente')->middleware('auth');
Route::get('cliente/ventas/{cliente}','ClienteController@ventasCliente')->name('ventasCliente')->middleware('auth');
Route::get('cliente/presupuestos/{cliente}','ClienteController@presupuestosCliente')->name('presupuestosCliente')->middleware('auth');
Route::get('cliente/presupuesto/quitar/{presupuesto}','ClienteController@quitarClientePresupuesto')->name('quitarCliente')->middleware('auth');
Route::put('clientes/modificar/{cliente}','ClienteController@update')->name('modificarCliente')->middleware('auth');

/* Entrega */

Route::delete('cliente/entrega/borrar/{entrega}','EntregaController@destroy')->name('borrarEntrega')->middleware('auth');


/* Notificacion */
Route::get('obtenerventa/{venta}','NotificacionController@obtenerVenta')->name('obtenerVenta')->middleware('auth');
Route::get('ventas/obtenerventa/{venta}','NotificacionController@obtenerVenta')->name('obtenerVenta')->middleware('auth');

Route::post('ventas/pendientes/notificacion','NotificacionController@store')->name('guardarNotificacion')->middleware('auth');
Route::delete('ventas/pendientes/notificacion/borrar/{venta}','NotificacionController@destroy')->name('borrarNotificacion')->middleware('auth');